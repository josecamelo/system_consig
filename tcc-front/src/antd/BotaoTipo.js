/* @author Gamarra - francisco.gamarra@gmail.com */
/* react-web-antd */
export default class BotaoTipo {

	s;

	constructor(s) {
		this.s = s;
	}

	toString() {
		return this.s;
	}
}
BotaoTipo.primary = new BotaoTipo("primary");
BotaoTipo.danger = new BotaoTipo("danger");
BotaoTipo.link = new BotaoTipo("link");
