/* @author Gamarra - francisco.gamarra@gmail.com */
/* react-web-antd */
import React from 'react';
import AFormItem from './AFormItem';
import Color from '../../app/misc/consts/fixeds/Color';
import Style from '../../app/misc/utils/Style';
import {Input} from 'antd';

export default class Select2 extends AFormItem {

	getBody(bindParam, idComponent, error) {
		return (
			<Input
				placeholder={bindParam.getLabel()}
				value={bindParam.asString()}
				onChange={e => bindParam.setCast(e.target.value)}
				style={error ? Select2.BORDER_ERROR : null}
				id={"input-" + idComponent}
			/>
		);
	}
}
Select2.BORDER_ERROR = Style.create().borderColor(Color.red).get();
