/* @author Gamarra - francisco.gamarra@gmail.com */
/* react-web-antd */
import React from 'react';
import CommonStyles from '../../app/misc/styles/CommonStyles';
import SuperComponent from '../../app/misc/components/SuperComponent';
import UCommons from '../../app/misc/utils/UCommons';
import {message} from 'antd';

export default class AfterString extends SuperComponent {

	constructor(props) {
		super(props);

	}

	message;

	static instance;

	static get() {
		if (UCommons.isEmpty(AfterString.instance)) {
			AfterString.instance = <AfterString/>;
		}
		return AfterString.instance;
	}

	render() {
		return (
			<span
			onClick={e => message.info("Significa que o campo aceita caracteres alfa numericos")}
			style={AfterString.STYLE_ICON}
			>A</span>
		);
	}
}
AfterString.STYLE_ICON = CommonStyles.POINTER.get();

AfterString.defaultProps = SuperComponent.defaultProps;
