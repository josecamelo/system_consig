/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import BindingBoolean from '../../../app/campos/support/BindingBoolean';
import UCommons from '../../../app/misc/utils/UCommons';

export default class BindingObservacoes extends BindingBoolean {

	static count = 0;
	itens = [];

	constructor() {
		super("Observacoes");
	}

	add(o) {
		if (UCommons.isEmpty(o.getId())) {
			BindingObservacoes.count--;
			o.setId(BindingObservacoes.count);
			this.itens.add(o);
		} else {
			let obj = this.itens.byId(o.getId());
			if (UCommons.isEmpty(obj)) {
				this.itens.add(o);
			} else {
				obj.setDataHora(null);
				obj.setUsuario(null);
				obj.setTexto(o.getTexto());
				obj.setAnexo(o.getAnexo());
			}
		}
		this.notifyObservers();
	}
	remove(o) {
		o = this.itens.byId(o.getId());
		this.itens.removeObject(o);
		this.notifyObservers();
	}
	clear() {
		this.itens = [];
	}
	getItens() {
		return this.itens;
	}

}
