/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import BindingString from '../../../app/campos/support/BindingString';

export default class BindingObservacao extends BindingString {

	constructor() {
		super("Observação", 250);
	}

}
