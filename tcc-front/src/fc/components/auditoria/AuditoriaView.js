/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import React from 'react';
import Color from '../../../app/misc/consts/fixeds/Color';
import Coluna from '../tabela/Coluna';
import LayoutApp from '../LayoutApp';
import Loading from '../../../antd/Loading';
import SuperComponent from '../../../app/misc/components/SuperComponent';
import Tabela from '../tabela/Tabela';
import TextAlign from '../../../app/misc/consts/enums/TextAlign';
import UInteger from '../../../app/misc/utils/UInteger';
import UString from '../../../app/misc/utils/UString';

export default class AuditoriaView extends SuperComponent {

	render() {
		if (this.props.campos.auditoria.carregado()) {
			return this.render0();
		} else if (UString.notEmpty(this.props.campos.auditoria.getMensagemErro())) {
			const s = "Ocorreu um erro no servico: " + this.props.campos.auditoria.getMensagemErro();
			return <span style={AuditoriaView.STYLE_ERROR.get()}>{s}</span>;
		} else {
			setTimeout(() => this.props.campos.auditoria.carregar(), 250);
			return <Loading/>;
		}
	}

	render0() {
		return (
			<Tabela
			bind={this.props.campos.auditoria}
			colunas={AuditoriaView.COLUNAS}
			onClick={o => this.edit(o)}
			funcGetEditIcon={o => {
				if (UInteger.equals(o.idTipo, 2)) {
					return Tabela.ICONE_SEARCH;
				} else {
					return Tabela.ICONE_VAZIO;
				}
			}}
			/>
		);
	}

	edit(o) {
		if (UInteger.equals(o.idTipo, 2)) {
			this.props.campos.detalharAuditoria(o);
		}
	}

	componentDidMount() {
		this.observar(this.props.campos.auditoria);
	}
}
AuditoriaView.TIPO = new Coluna(15, "Ação", o => o.tipo, TextAlign.center).setId("AuditoriaView-Cols-tipo");
AuditoriaView.USUARIO = new Coluna(34, "Usuário", o => o.usuario, TextAlign.center).setId("AuditoriaView-Cols-usuario");
AuditoriaView.DATA = new Coluna(15, "Data/Hora", o => o.data, TextAlign.center).setId("AuditoriaView-Cols-data");
AuditoriaView.TEMPO = new Coluna(15, "Tempo", o => o.tempo, TextAlign.center).setId("AuditoriaView-Cols-tempo");
AuditoriaView.COLUNAS = [AuditoriaView.TIPO, AuditoriaView.USUARIO, AuditoriaView.DATA, AuditoriaView.TEMPO];
AuditoriaView.STYLE_ERROR = LayoutApp.createStyle().margin(10).bold(true).color(Color.red);

AuditoriaView.defaultProps = SuperComponent.defaultProps;
