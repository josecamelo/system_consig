/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import BindingBooleanFiltro from '../../app/campos/support/BindingBooleanFiltro';
import BindingCpf from '../../app/campos/support/BindingCpf';
import BindingData from '../../app/campos/support/BindingData';
import BindingDecimal from '../../app/campos/support/BindingDecimal';
import BindingEmail from '../../app/campos/support/BindingEmail';
import BindingInteger from '../../app/campos/support/BindingInteger';
import BindingList from '../../app/campos/support/BindingList';
import BindingNomeProprio from '../../app/campos/support/BindingNomeProprio';
import BindingString from '../../app/campos/support/BindingString';
import BindingTelefone from '../../app/campos/support/BindingTelefone';
import UCommons from '../../app/misc/utils/UCommons';

export default class Filtros {

	dataSource = [];
	list = [];
	busca;

	init() {}

	getDataSource() {
		return this.dataSource;
	}

	setDataSource(value) {
		if (UCommons.isEmpty(value)) {
			this.dataSource = [];
		} else {
			this.dataSource = value;
		}
	}

	getBusca() {
		return this.busca;
	}

	constructor() {
		this.busca = this.newString("busca", 200);
	}

	touch() {
		this.list.forEach(campo => campo.setVirgin(false));
	}

	getErros() {
		return this.list.filter(campo => !campo.isValid() && !campo.isVirgin());
	}
	add(campo) {
		this.list.add(campo);
		return campo;
	}
	newString(nomeP, size) {
		return this.add(new BindingString(nomeP, size));
	}
	newCpf(nomeP) {
		return this.add(new BindingCpf(nomeP));
	}
	newEmail(nomeP) {
		return this.add(new BindingEmail(nomeP));
	}
	newTelefone(nomeP) {
		return this.add(new BindingTelefone(nomeP));
	}
	newNomeProprio(nomeP) {
		return this.add(new BindingNomeProprio(nomeP));
	}
	newListFixed(nomeP, itens) {
		return this.add(new BindingList(nomeP).setItens(itens));
	}
	newFk(nomeP) {
		return this.add(new BindingList(nomeP));
	}
	newStatus(nomeP) {
		return this.add(new BindingList(nomeP));
	}
	newBoolean(nomeP) {
		return this.add(new BindingBooleanFiltro(nomeP));
	}
	newImagem(nomeP) {
		return this.add(new BindingString(nomeP, 100));
	}
	newData(nomeP) {
		return this.add(new BindingData(nomeP));
	}
	newInteger(nomeP) {
		return this.add(new BindingInteger(nomeP, 99999));
	}
	newDecimal(nomeP) {
		return this.add(new BindingDecimal(nomeP, 4));
	}

}
