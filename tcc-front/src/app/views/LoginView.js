/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import React from 'react';
import FcBotao from '../../fc/components/FcBotao';
import Foreach from '../misc/components/Foreach';
import FormItemCheck from '../../antd/form/FormItemCheck';
import FormItemInput from '../../antd/form/FormItemInput';
import FormKeyDownObserver from '../../fc/components/FormKeyDownObserver';
import LayoutApp from '../../fc/components/LayoutApp';
import Resources from '../../resources/Resources';
import Session from '../estado/Session';
import Style from '../misc/utils/Style';
import {Card} from 'antd';
import {Row} from 'antd';

export default class LoginView extends FormKeyDownObserver {

	render() {
		const session = Session.getInstance();
		return (
			<div style={LoginView.FORM_STYLE}>
				<Card size={"small"} style={LoginView.CARD_STYLE} className={"card-table"}>
					<Row gutter={10}>
						<div style={LoginView.ALIGN_ITEMS_CENTER}>
							<img src={Resources.user} alt={Resources.user} style={LoginView.IMAGE_STYLE}/>
						</div>
					</Row>
					<Row gutter={10}>
						<div style={LoginView.ALIGN_ITEMS_CENTER}>
							<span style={LoginView.TEXT_STYLE.get()}>Por favor, autentique-se!</span>
						</div>
					</Row>
					<Row gutter={10}>
						<FormItemInput bind={session.login} lg={24}/>
					</Row>
					<Row gutter={10}>
						<FormItemInput bind={session.senha} lg={24}/>
					</Row>
					<Row gutter={10}>
						<FormItemCheck bind={session.lembrarme} lg={24}/>
					</Row>
					<Row gutter={10}>
						<div style={LoginView.ALIGN_ITEMS_CENTER}>
							<FcBotao title={"Efetuar Login"} onClick={() => session.efetuaLogin()}/>
						</div>
					</Row>
				</Card>
				{this.botoesLogin(session)}
			</div>
		);
	}

	static LOGINS = ["admin@admin.com","atendente1@tcc.com"];

	botoesLogin(session) {
		return (
			<Card size={"small"} style={LoginView.CARD_STYLE} className={"card-table"}>
				<Foreach array={LoginView.LOGINS} func={o =>
					<Row gutter={10}>
						<div style={LoginView.ALIGN_ITEMS_CENTER}>
							<FcBotao title={o} onClick={() => {
								session.login.set(o);
								session.senha.set("123456");
								session.efetuaLogin();
							}}/>
						</div>
					</Row>
				}/>
			</Card>
		);

	}

	onKeyDown0(e) {
		if (e.enter()) {
			const session = Session.getInstance();
			session.efetuaLogin();
		}
	}

}
LoginView.TEXT_STYLE = Style.create();
LoginView.IMAGE_STYLE = Style.create().height(100).get();
LoginView.FORM_STYLE = Style.create().width(500).margin("auto").padding(30).get();
LoginView.ALIGN_ITEMS_CENTER = Style.create().textAlignCenter().alignItemsCenter().widthPercent(100).paddingBottom(20).get();
LoginView.CARD_STYLE = LayoutApp.createStyle().marginTop(15).padding(20).get();

LoginView.defaultProps = FormKeyDownObserver.defaultProps;
