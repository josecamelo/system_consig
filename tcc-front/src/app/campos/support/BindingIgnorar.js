/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
import BindingBoolean from './BindingBoolean';

export default class BindingIgnorar extends BindingBoolean {

	bind;

	constructor(label, bind) {
		super(label);
		this.bind = bind;
	}

	afterSet() {
		this.bind.setDisabled(this.isTrue());
	}

}
