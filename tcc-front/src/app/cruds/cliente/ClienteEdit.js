/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import ClienteEditAbstract from '../../auto/cliente/ClienteEditAbstract';

export default class ClienteEdit extends ClienteEditAbstract {
	setWidthForm = o => this.setState({widthForm:o});
	setAbaSelecionada = o => this.setState({abaSelecionada:o});}

ClienteEdit.defaultProps = ClienteEditAbstract.defaultProps;
