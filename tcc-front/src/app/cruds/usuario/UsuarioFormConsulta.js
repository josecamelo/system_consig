/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import UsuarioFormConsultaAbstract from '../../auto/usuario/UsuarioFormConsultaAbstract';

export default class UsuarioFormConsulta extends UsuarioFormConsultaAbstract {
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});}

UsuarioFormConsulta.defaultProps = UsuarioFormConsultaAbstract.defaultProps;
