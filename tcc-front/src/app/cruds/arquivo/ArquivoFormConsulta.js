/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import ArquivoFormConsultaAbstract from '../../auto/arquivo/ArquivoFormConsultaAbstract';

export default class ArquivoFormConsulta extends ArquivoFormConsultaAbstract {
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});}

ArquivoFormConsulta.defaultProps = ArquivoFormConsultaAbstract.defaultProps;
