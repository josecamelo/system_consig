/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import ClienteStatusFormConsultaAbstract from '../../auto/clienteStatus/ClienteStatusFormConsultaAbstract';

export default class ClienteStatusFormConsulta extends ClienteStatusFormConsultaAbstract {
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowImportarArquivo = o => this.setState({showImportarArquivo:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});}

ClienteStatusFormConsulta.defaultProps = ClienteStatusFormConsultaAbstract.defaultProps;
