/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import ImportacaoArquivoStatusAbstract from '../../auto/importacaoArquivoStatus/ImportacaoArquivoStatusAbstract';

export default class ImportacaoArquivoStatus extends ImportacaoArquivoStatusAbstract {}
