/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import ImportacaoArquivoStatusFormConsultaAbstract from '../../auto/importacaoArquivoStatus/ImportacaoArquivoStatusFormConsultaAbstract';

export default class ImportacaoArquivoStatusFormConsulta extends ImportacaoArquivoStatusFormConsultaAbstract {
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});}

ImportacaoArquivoStatusFormConsulta.defaultProps = ImportacaoArquivoStatusFormConsultaAbstract.defaultProps;
