/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import ImportacaoArquivoErroAbstract from '../../auto/importacaoArquivoErro/ImportacaoArquivoErroAbstract';

export default class ImportacaoArquivoErro extends ImportacaoArquivoErroAbstract {}
