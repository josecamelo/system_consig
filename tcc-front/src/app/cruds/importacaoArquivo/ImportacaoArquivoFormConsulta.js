/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import ImportacaoArquivoFormConsultaAbstract from '../../auto/importacaoArquivo/ImportacaoArquivoFormConsultaAbstract';

export default class ImportacaoArquivoFormConsulta extends ImportacaoArquivoFormConsultaAbstract {
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});}

ImportacaoArquivoFormConsulta.defaultProps = ImportacaoArquivoFormConsultaAbstract.defaultProps;
