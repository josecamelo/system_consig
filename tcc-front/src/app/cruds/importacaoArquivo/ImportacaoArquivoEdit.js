/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import React from 'react';
import FcBotao from '../../../fc/components/FcBotao';
import ImportacaoArquivoEditAbstract from '../../auto/importacaoArquivo/ImportacaoArquivoEditAbstract';

export default class ImportacaoArquivoEdit extends ImportacaoArquivoEditAbstract {

	getBotoesCustomizados() {
		return <FcBotao title={"Processar"} onClick={() => this.getCampos().processar()}/>;
	}
	setWidthForm = o => this.setState({widthForm:o});
	setAbaSelecionada = o => this.setState({abaSelecionada:o});

}

ImportacaoArquivoEdit.defaultProps = ImportacaoArquivoEditAbstract.defaultProps;
