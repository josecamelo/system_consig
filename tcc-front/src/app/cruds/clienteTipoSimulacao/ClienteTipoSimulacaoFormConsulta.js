/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import ClienteTipoSimulacaoFormConsultaAbstract from '../../auto/clienteTipoSimulacao/ClienteTipoSimulacaoFormConsultaAbstract';

export default class ClienteTipoSimulacaoFormConsulta extends ClienteTipoSimulacaoFormConsultaAbstract {
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowImportarArquivo = o => this.setState({showImportarArquivo:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});}

ClienteTipoSimulacaoFormConsulta.defaultProps = ClienteTipoSimulacaoFormConsultaAbstract.defaultProps;
