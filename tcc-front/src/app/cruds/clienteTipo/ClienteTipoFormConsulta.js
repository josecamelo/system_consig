/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import ClienteTipoFormConsultaAbstract from '../../auto/clienteTipo/ClienteTipoFormConsultaAbstract';

export default class ClienteTipoFormConsulta extends ClienteTipoFormConsultaAbstract {
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowImportarArquivo = o => this.setState({showImportarArquivo:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});}

ClienteTipoFormConsulta.defaultProps = ClienteTipoFormConsultaAbstract.defaultProps;
