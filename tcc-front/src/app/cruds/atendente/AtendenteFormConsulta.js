/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import AtendenteFormConsultaAbstract from '../../auto/atendente/AtendenteFormConsultaAbstract';

export default class AtendenteFormConsulta extends AtendenteFormConsultaAbstract {
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});}

AtendenteFormConsulta.defaultProps = AtendenteFormConsultaAbstract.defaultProps;
