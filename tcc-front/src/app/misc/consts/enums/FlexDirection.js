/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class FlexDirection {
	static column = 'column';
	static row = 'row';
	static flex = 'flex';
}
