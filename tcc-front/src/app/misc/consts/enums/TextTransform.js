/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class TextTransform {
	static none = 'none';
	static uppercase = 'uppercase';
	static lowercase = 'lowercase';
	static capitalize = 'capitalize';
}
