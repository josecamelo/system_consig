/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class Overflow {
	static visible = 'visible';
	static hidden = 'hidden';
	static scroll = 'scroll';
	static auto = 'auto';
	static initial = 'initial';
	static inherit = 'inherit';
}
