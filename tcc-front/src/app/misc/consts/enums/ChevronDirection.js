/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class ChevronDirection {
	static down = 'down';
	static forward = 'forward';
}
