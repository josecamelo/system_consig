/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class Display {
	static none = 'none';
	static flex = 'flex';
	static block = 'block';
}
