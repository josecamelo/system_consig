/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class RequestType {
	static get = 'get';
	static post = 'post';
}
