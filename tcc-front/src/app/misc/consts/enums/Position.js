/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class Position {
	static absolute = 'absolute';
	static relative = 'relative';
	static fixed = 'fixed';
}
