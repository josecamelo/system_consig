/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class GridWrap {
	static nowrap = 'nowrap';
	static wrap = 'wrap';
	static wrapReverse = 'wrap-reverse';
}
