/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class IconeStatus {
	static none = 'none';
	static ok = 'ok';
	static warning = 'warning';
	static error = 'error';
	static cadeado = 'cadeado';
}
