/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class TextAlign {
	static center = 'center';
	static right = 'right';
	static justify = 'justify';
	static left = 'left';
}
