/* @author Gamarra - francisco.gamarra@gmail.com */
/* react */
export default class AlignItens {
	static flexStart = 'flex-start';
	static center = 'center';
	static flexEnd = 'flex-end';
	static stretch = 'stretch';
}
