/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import AtendenteConsulta from '../../cruds/atendente/AtendenteConsulta';
import AtendenteUtils from '../../cruds/atendente/AtendenteUtils';
import Binding from '../../campos/support/Binding';
import EntityCampos from '../../../fc/components/EntityCampos';
import Sessao from '../../../projeto/Sessao';
import UCommons from '../../misc/utils/UCommons';

export default class AtendenteCamposAbstract extends EntityCampos {

	nome;
	email;
	original;
	to;
	getEntidade() {
		return "Atendente";
	}
	getEntidadePath() {
		return "atendente";
	}
	initImpl() {
		this.nome = this.newNomeProprio("Nome", true, "Geral");
		this.email = this.newEmail("E-mail", true, "Geral");
		this.init2();
		this.construido = true;
	}
	setCampos(o) {
		if (UCommons.isEmpty(o)) {
			throw new Error("o === null");
		}
		this.checkInstance();
		Binding.notificacoesDesligadasInc();
		this.disabledObservers = true;
		this.original = o;
		this.to = AtendenteUtils.getInstance().clonar(o);
		this.id.clear();
		this.id.set(this.to.getId());
		this.email.set(this.to.getEmail());
		this.nome.set(this.to.getNome());
		this.excluido.set(this.to.getExcluido());
		this.registroBloqueado.set(this.to.getRegistroBloqueado());
		this.id.setStartValue(this.to.getId());
		this.email.setStartValue(this.email.get());
		this.nome.setStartValue(this.nome.get());
		this.excluido.setStartValue(this.excluido.get());
		this.registroBloqueado.setStartValue(this.registroBloqueado.get());
		let readOnly = this.isReadOnly();
		this.email.setDisabled(readOnly);
		this.nome.setDisabled(readOnly);
		this.setCampos2(o);
		this.reiniciar();
	}
	setCampos2(o) {}
	getTo() {
		this.checkInstance();
		this.to.setId(this.id.get());
		this.to.setEmail(this.email.get());
		this.to.setNome(this.nome.get());
		this.to.setExcluido(this.excluido.get());
		this.to.setRegistroBloqueado(this.registroBloqueado.get());
		return this.to;
	}
	setJson(obj) {
		this.checkInstance();
		let json = obj;
		let o = AtendenteUtils.getInstance().fromJson(json);
		this.setCampos(o);
		let itensGrid = AtendenteConsulta.getInstance().getDataSource();
		if (UCommons.notEmpty(itensGrid)) {
			let itemGrid = itensGrid.byId(o.getId());
			if (UCommons.notEmpty(itemGrid)) {
				AtendenteUtils.getInstance().merge(o, itemGrid);
			}
		}
		return o;
	}
	init2() {}
	static getText(o) {
		if (UCommons.isEmpty(o)) {
			return null;
		}
		return o.getNome();
	}
	observacoesEdit(o) {
		throw new Error("???");
	}
	houveMudancas() {
		if (UCommons.isEmpty(this.original)) {
			return false;
		}
		return !AtendenteUtils.getInstance().equals(this.original, this.getTo());
	}
	camposAlterados() {
		return AtendenteUtils.getInstance().camposAlterados(this.original, this.getTo());
	}
	cancelarAlteracoes() {
		this.setCampos(this.original);
	}
	getOriginal() {
		return this.original;
	}
	checkInstance() {
		Sessao.checkInstance("AtendenteCampos", this);
	}
}
