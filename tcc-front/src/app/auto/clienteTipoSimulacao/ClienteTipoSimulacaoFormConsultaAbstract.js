/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import React from 'react';
import ClienteTipoSimulacaoCampos from '../../cruds/clienteTipoSimulacao/ClienteTipoSimulacaoCampos';
import ClienteTipoSimulacaoCols from '../../cruds/clienteTipoSimulacao/ClienteTipoSimulacaoCols';
import ClienteTipoSimulacaoConsulta from '../../cruds/clienteTipoSimulacao/ClienteTipoSimulacaoConsulta';
import ClienteTipoSimulacaoEdit from '../../cruds/clienteTipoSimulacao/ClienteTipoSimulacaoEdit';
import ClienteTipoSimulacaoUtils from '../../cruds/clienteTipoSimulacao/ClienteTipoSimulacaoUtils';
import FormConsulta from '../../../fc/components/FormConsulta';
import FormItemConsulta from '../../../fc/components/FormItemConsulta';
import Tabela from '../../../fc/components/tabela/Tabela';
import {Fragment} from 'react';

export default class ClienteTipoSimulacaoFormConsultaAbstract extends FormConsulta {

	normalCols;
	grupoCols;
	componentDidMount3() {
		this.titulo = "Cliente Tipo Simulação";
		this.componentDidMount4();
		this.normalCols = ClienteTipoSimulacaoCols.getInstance().list;
		this.grupoCols = ClienteTipoSimulacaoCols.getInstance().grupos;
	}
	componentDidMount4() {}
	getFiltros() {
		return ClienteTipoSimulacaoConsulta.getInstance();
	}
	getRenderFiltros() {
		const campos = this.getFiltros();
		return (
			<Fragment>
				<FormItemConsulta bind1={campos.nome}/>
				<FormItemConsulta bind1={campos.excluido} bind2={campos.registroBloqueado}/>
			</Fragment>
		);
	}
	novo() {
		this.setEdit(ClienteTipoSimulacaoUtils.getInstance().novo());
	}
	edit(id) {
		ClienteTipoSimulacaoCampos.getInstance().edit(id, o => this.setEdit(o));
	}
	setEdit(o) {
		ClienteTipoSimulacaoCampos.getInstance().setCampos(o);
		this.setShowEdit(true);
	}
	renderEdit() {
		return <ClienteTipoSimulacaoEdit somenteUpdate={true} onClose={() => this.setShowEdit(false)} isModal={true}/>;
	}
	getTable() {
		const cps = ClienteTipoSimulacaoConsulta.getInstance();
		return (
			<Tabela
			bind={cps.dados}
			colunas={this.normalCols}
			colunasGrupo={this.grupoCols}
			onClick={o => this.edit(o.getId())}
			formKeyDown={this}
			/>
		);
	}
	getEntidade() {
		return "ClienteTipoSimulacao";
	}
	getEntidadePath() {
		return "cliente-tipo-simulacao";
	}
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowImportarArquivo = o => this.setState({showImportarArquivo:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});
}

ClienteTipoSimulacaoFormConsultaAbstract.defaultProps = FormConsulta.defaultProps;
