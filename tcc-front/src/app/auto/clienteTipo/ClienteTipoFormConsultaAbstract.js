/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import React from 'react';
import ClienteTipoCampos from '../../cruds/clienteTipo/ClienteTipoCampos';
import ClienteTipoCols from '../../cruds/clienteTipo/ClienteTipoCols';
import ClienteTipoConsulta from '../../cruds/clienteTipo/ClienteTipoConsulta';
import ClienteTipoEdit from '../../cruds/clienteTipo/ClienteTipoEdit';
import ClienteTipoUtils from '../../cruds/clienteTipo/ClienteTipoUtils';
import FormConsulta from '../../../fc/components/FormConsulta';
import FormItemConsulta from '../../../fc/components/FormItemConsulta';
import Tabela from '../../../fc/components/tabela/Tabela';
import {Fragment} from 'react';

export default class ClienteTipoFormConsultaAbstract extends FormConsulta {

	normalCols;
	grupoCols;
	componentDidMount3() {
		this.titulo = "Cliente Tipo";
		this.componentDidMount4();
		this.normalCols = ClienteTipoCols.getInstance().list;
		this.grupoCols = ClienteTipoCols.getInstance().grupos;
	}
	componentDidMount4() {}
	getFiltros() {
		return ClienteTipoConsulta.getInstance();
	}
	getRenderFiltros() {
		const campos = this.getFiltros();
		return (
			<Fragment>
				<FormItemConsulta bind1={campos.nome}/>
				<FormItemConsulta bind1={campos.excluido} bind2={campos.registroBloqueado}/>
			</Fragment>
		);
	}
	novo() {
		this.setEdit(ClienteTipoUtils.getInstance().novo());
	}
	edit(id) {
		ClienteTipoCampos.getInstance().edit(id, o => this.setEdit(o));
	}
	setEdit(o) {
		ClienteTipoCampos.getInstance().setCampos(o);
		this.setShowEdit(true);
	}
	renderEdit() {
		return <ClienteTipoEdit somenteUpdate={true} onClose={() => this.setShowEdit(false)} isModal={true}/>;
	}
	getTable() {
		const cps = ClienteTipoConsulta.getInstance();
		return (
			<Tabela
			bind={cps.dados}
			colunas={this.normalCols}
			colunasGrupo={this.grupoCols}
			onClick={o => this.edit(o.getId())}
			formKeyDown={this}
			/>
		);
	}
	getEntidade() {
		return "ClienteTipo";
	}
	getEntidadePath() {
		return "cliente-tipo";
	}
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowImportarArquivo = o => this.setState({showImportarArquivo:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});
}

ClienteTipoFormConsultaAbstract.defaultProps = FormConsulta.defaultProps;
