/* @author Gamarra - francisco.gamarra@gmail.com */
/* tcc-java */
import React from 'react';
import ClienteStatusCampos from '../../cruds/clienteStatus/ClienteStatusCampos';
import ClienteStatusCols from '../../cruds/clienteStatus/ClienteStatusCols';
import ClienteStatusConsulta from '../../cruds/clienteStatus/ClienteStatusConsulta';
import ClienteStatusEdit from '../../cruds/clienteStatus/ClienteStatusEdit';
import ClienteStatusUtils from '../../cruds/clienteStatus/ClienteStatusUtils';
import FormConsulta from '../../../fc/components/FormConsulta';
import FormItemConsulta from '../../../fc/components/FormItemConsulta';
import Tabela from '../../../fc/components/tabela/Tabela';
import {Fragment} from 'react';

export default class ClienteStatusFormConsultaAbstract extends FormConsulta {

	normalCols;
	grupoCols;
	componentDidMount3() {
		this.titulo = "Cliente Status";
		this.componentDidMount4();
		this.normalCols = ClienteStatusCols.getInstance().list;
		this.grupoCols = ClienteStatusCols.getInstance().grupos;
	}
	componentDidMount4() {}
	getFiltros() {
		return ClienteStatusConsulta.getInstance();
	}
	getRenderFiltros() {
		const campos = this.getFiltros();
		return (
			<Fragment>
				<FormItemConsulta bind1={campos.nome}/>
				<FormItemConsulta bind1={campos.excluido} bind2={campos.registroBloqueado}/>
			</Fragment>
		);
	}
	novo() {
		this.setEdit(ClienteStatusUtils.getInstance().novo());
	}
	edit(id) {
		ClienteStatusCampos.getInstance().edit(id, o => this.setEdit(o));
	}
	setEdit(o) {
		ClienteStatusCampos.getInstance().setCampos(o);
		this.setShowEdit(true);
	}
	renderEdit() {
		return <ClienteStatusEdit somenteUpdate={true} onClose={() => this.setShowEdit(false)} isModal={true}/>;
	}
	getTable() {
		const cps = ClienteStatusConsulta.getInstance();
		return (
			<Tabela
			bind={cps.dados}
			colunas={this.normalCols}
			colunasGrupo={this.grupoCols}
			onClick={o => this.edit(o.getId())}
			formKeyDown={this}
			/>
		);
	}
	getEntidade() {
		return "ClienteStatus";
	}
	getEntidadePath() {
		return "cliente-status";
	}
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowImportarArquivo = o => this.setState({showImportarArquivo:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});
}

ClienteStatusFormConsultaAbstract.defaultProps = FormConsulta.defaultProps;
