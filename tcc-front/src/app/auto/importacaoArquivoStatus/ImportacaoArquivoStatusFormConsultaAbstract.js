/* @author Gamarra - francisco.gamarra@gmail.com */
/* front-constructor */
import React from 'react';
import EntityCampos from '../../../fc/components/EntityCampos';
import FormConsulta from '../../../fc/components/FormConsulta';
import FormItemConsulta from '../../../fc/components/FormItemConsulta';
import ImportacaoArquivoStatusCampos from '../../cruds/importacaoArquivoStatus/ImportacaoArquivoStatusCampos';
import ImportacaoArquivoStatusCols from '../../cruds/importacaoArquivoStatus/ImportacaoArquivoStatusCols';
import ImportacaoArquivoStatusConsulta from '../../cruds/importacaoArquivoStatus/ImportacaoArquivoStatusConsulta';
import ImportacaoArquivoStatusEdit from '../../cruds/importacaoArquivoStatus/ImportacaoArquivoStatusEdit';
import ImportacaoArquivoStatusUtils from '../../cruds/importacaoArquivoStatus/ImportacaoArquivoStatusUtils';
import Tabela from '../../../fc/components/tabela/Tabela';
import {Fragment} from 'react';

export default class ImportacaoArquivoStatusFormConsultaAbstract extends FormConsulta {

	normalCols;
	grupoCols;
	componentDidMount3() {
		this.titulo = "Importação Arquivo Status";
		this.componentDidMount4();
		this.normalCols = ImportacaoArquivoStatusCols.getInstance().list;
		this.grupoCols = ImportacaoArquivoStatusCols.getInstance().grupos;
	}
	componentDidMount4() {}
	getFiltros() {
		return ImportacaoArquivoStatusConsulta.getInstance();
	}
	getRenderFiltros() {
		const campos = this.getFiltros();
		return (
			<Fragment>
				<FormItemConsulta bind1={campos.nome}/>
				<FormItemConsulta bind1={campos.excluido} bind2={campos.registroBloqueado}/>
			</Fragment>
		);
	}
	novo() {
		this.setEdit(ImportacaoArquivoStatusUtils.getInstance().novo());
	}
	edit(id) {
		ImportacaoArquivoStatusCampos.getInstance().edit(id, o => this.setEdit(o));
	}
	setEdit(o) {
		ImportacaoArquivoStatusCampos.getInstance().setCampos(o);
		this.setShowEdit(true);
	}
	renderEdit() {
		return <ImportacaoArquivoStatusEdit onDelete={idP => this.delete(idP)} onClose={() => this.setShowEdit(false)} isModal={true}/>;
	}
	delete(idP) {
		if (!this.permissao.delete()) return;
		EntityCampos.excluir(ImportacaoArquivoStatusCampos.getInstance().getEntidadePath(), idP, () => {
			let cps = ImportacaoArquivoStatusConsulta.getInstance();
			let o = cps.dados.getItens().byId(idP);
			cps.dados.remove(o);
		});
	}
	onDeleteFunction() {
		if (!this.permissao.delete()) return null;
		return o => this.delete(o.getId());
	}
	getTable() {
		const cps = ImportacaoArquivoStatusConsulta.getInstance();
		return (
			<Tabela
			bind={cps.dados}
			colunas={this.normalCols}
			colunasGrupo={this.grupoCols}
			onClick={o => this.edit(o.getId())}
			onDelete={this.onDeleteFunction()}
			formKeyDown={this}
			/>
		);
	}
	getEntidade() {
		return "ImportacaoArquivoStatus";
	}
	getEntidadePath() {
		return "importacao-arquivo-status";
	}
	setWidthForm = o => this.setState({widthForm:o});
	setShowEdit = o => this.setState({showEdit:o});
	setShowImportarArquivo = o => this.setState({showImportarArquivo:o});
	setMaisFiltros = o => this.setState({maisFiltros:o});
}

ImportacaoArquivoStatusFormConsultaAbstract.defaultProps = FormConsulta.defaultProps;
