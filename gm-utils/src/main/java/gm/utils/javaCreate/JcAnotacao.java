package gm.utils.javaCreate;

import java.lang.annotation.Annotation;

import gm.utils.string.UString;
import lombok.Getter;

@Getter
public class JcAnotacao {
	private final JcTipos tipos = new JcTipos();
	private JcTipo tipo;
	private String parametros;
	public JcAnotacao(final String tipo, final String parametros) {
		this(new JcTipo(tipo), parametros);
	}
	public JcAnotacao(final JcTipo tipo, final String parametros) {
		if (parametros != null) {
			this.parametros = "\"" + parametros + "\"";
		}
		this.setTipo(tipo);
	}
	public JcAnotacao(final JcTipo tipo) {
		this(tipo, null);
	}
	public JcAnotacao(final Class<? extends Annotation> classe, final String parametros) {
		this(new JcTipo(classe), parametros);
	}
	public JcAnotacao(final Class<? extends Annotation> classe) {
		this(classe, null);
	}
	public void setTipo(final Class<? extends Annotation> classe) {
		this.setTipo(new JcTipo(classe));
	}
	public void setTipo(final JcTipo tipo) {
		if (this.tipo != null) {
			this.tipos.remove(this.tipo);
		}
		this.tipo = tipo;
		this.tipos.add(tipo);
	}
	@Override
	public String toString() {
		String s = "@" + this.tipo.toString();
		if (UString.notEmpty(this.parametros)) {
			s += "("+this.parametros+")";
		}
		return s;
	}
	
	public JcAnotacao addParametro(final String key, final Object value) {
		final String s = key+"="+ value;
		if (UString.isEmpty(this.parametros)) {
			this.parametros = s;
		} else {
			this.parametros += ", " + s;
		}
		return this;
	}
	public JcAnotacao addImport(final Class<?> classe) {
		this.getTipos().add(classe);
		return this;
	}
}
