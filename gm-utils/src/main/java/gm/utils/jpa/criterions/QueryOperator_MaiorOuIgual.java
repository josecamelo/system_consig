package gm.utils.jpa.criterions;

import javax.persistence.criteria.Predicate;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class QueryOperator_MaiorOuIgual extends QueryOperator{
	public QueryOperator_MaiorOuIgual(String campo, Object value) {
		super(campo, value, false);
	}
	@Override
	protected Criterion criterion() {
		return Restrictions.ge(getCampo(), getValue());
	}
	@Override
	protected String nativo() {
		return getCampo() + " >= " + getNativeValue();
	}
	@Override
	protected Predicate getPredicateTrue(CriterioQuery<?> cq, String campo) {
		Number number = (Number) getValue();
		return cq.getCb().ge(cq.getPath().get(campo), number);
	}
}