package gm.utils.jpa.criterions;

import javax.persistence.criteria.Predicate;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class QueryOperator_MenorOuIgual extends QueryOperator{
	public QueryOperator_MenorOuIgual(String campo, Object value) {
		super(campo, value, false);
	}
	@Override
	protected Criterion criterion() {
		return Restrictions.le(getCampo(), getValue());
	}
	@Override
	protected String nativo() {
		return getCampo() + " <= " + getNativeValue();
	}
	@Override
	protected Predicate getPredicateTrue(CriterioQuery<?> cq, String campo) {
		Number number = (Number) getValue();
		return cq.getCb().le(cq.getPath().get(campo), number);
	}
}