package gm.utils.jpa.criterions;

import javax.persistence.criteria.Predicate;

public class QueryOperator_StringStartsWith extends QueryOperator_String {
	public QueryOperator_StringStartsWith(String campo, String value) {
		super(campo, value);
	}
	@Override
	public Object getValue() {
		return super.getValue() + "%";
	}
	@Override
	protected Predicate getPredicateTrue(CriterioQuery<?> cq, String campo) {
		String s = (String) getValue();
		s = s + "%";
		return cq.getCb().like(cq.getPath().get(campo), s);
	}
	@Override
	protected Predicate getPredicateFalse(CriterioQuery<?> cq, String campo) {
		String s = (String) getValue();
		s = s + "%";
		return cq.getCb().notLike(cq.getPath().get(campo), s);
	}
}