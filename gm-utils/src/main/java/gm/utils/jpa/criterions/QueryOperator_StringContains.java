package gm.utils.jpa.criterions;

import javax.persistence.criteria.Predicate;

import gm.utils.string.UString;

public class QueryOperator_StringContains extends QueryOperator_String {
	public QueryOperator_StringContains(String campo, String value) {
		super(campo, value);
	}
	@Override
	public Object getValue() {
		String s = UString.toString(super.getValue());
		s = s.replace(" ", "% %");
		return "%" + s + "%";
	}
	@Override
	protected Predicate getPredicateTrue(CriterioQuery<?> cq, String campo) {
		String s = (String) getValue();
		s = "%" + s + "%";
		return cq.getCb().like(cq.getPath().get(campo), s);
	}
	@Override
	protected Predicate getPredicateFalse(CriterioQuery<?> cq, String campo) {
		String s = (String) getValue();
		s = "%" + s + "%";
		return cq.getCb().notLike(cq.getPath().get(campo), s);
	}

}