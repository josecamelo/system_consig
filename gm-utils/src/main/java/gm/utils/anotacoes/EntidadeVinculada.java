package gm.utils.anotacoes;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/* uma entidade vinculada é uma entidade que só existe em razao de outra.
 * isso quer dizer que um registro pai é que dará origem a ela.
 * Se, por um acaso o campo do registro pai que aponta para esta for nulado,
 * o registro nesta será automaticamente excluido.
 * Se for preenchido, o registro nesta será automaticamente inserido
 *  */
@Retention(RetentionPolicy.RUNTIME)
public @interface EntidadeVinculada {
}