package gm.utils.comum;

import gm.utils.string.ListString;
import gm.utils.string.StringBox;
import gm.utils.string.UString;

public class UNomeProprio {

	private static final ListString nomesMasculinos = ListString.newFromArray(
	"Adriano","Alessandro","Alex","André","Antônio",
	"Bruno",
	"César","Caio",
	"Diego","Daniel","David",
	"Expedito","Emanuel","Ernesto",
	"Ferdinand","Fernando","Francisco","Frank",
	"Gustavo","Gabriel",
	"Hélio","Henrique","Heitor",
	"Isaias","Iuri",
	"João","José",
	"Kaio",
	"Leonardo",
	"Maicon","Marcos","Mario","Maurício","Maxuel","Maycon","Michael","Mike","Moises",
	"Natanael","Nóe","Nildo",
	"Osvaldo","Orlando","Otávio",
	"Pedro","Pietro",
	"Quintino",
	"Ricardo","Ruan",
	"Saulo","Sandro","Sérgio","Samoel",
	"Tiago","Tomé","Tomaz",
	"Ulisses",
	"Vicente","Valter","Vanderley","Vitor",
	"Washington","Waldemar","Walisson",
	"Xavier",
	"Yuri",
	"Zaqueu"
	);
	
	private static final ListString nomesFemininos = ListString.newFromArray(
	"Adriana","Alessandra","Aline","Ana",
	"Beatriz","Bárbara","Bianca","Bruna",
	"Carina","Carolina","Cristiane","Carla","Carmen",
	"Daniela","Débora","Diana",
	"Emanuela","Evelin","Édna","Elaine","Ester","Elana",
	"Fátima","Fernanda","Francisca",
	"Gabriela",
	"Joana","Janaína","Jéssica",
	"Karem",
	"Lídia","Luciana","Laura","Luana",
	"Marcela","Maria","Marta","Miriã","Mirian","Maura",
	"Nádia","Neuma","Noemi",
	"Otaviana","Olga",
	"Patrícia","Priscila","Paloma",
	"Rosane","Rosângela",
	"Suélen","Sabrina","Sara","Sabrina",
	"Tatiana","Telma","Taís",
	"Valéria","Valentina","Vanessa","Vívian",
	"Xaiane",
	"Yara",
	"Zélia","Zaíra","Zuleica","Zenilda"
	);
	
	private static final ListString nomes = nomesMasculinos.union(nomesFemininos).sort();
	
	private static final ListString sobrenomes = ListString.newFromArray(
	"Alves","Azevedo",
	"Borges","Beltrão",
	"Cabral","Caetano","Camargo","Carvalho","Correia",
	"Dias","Durval","Dalas",
	"Ferreira","Fernandes",
	"Gamarra","Godoi",
	"Lima","Lisboa",
	"Marcondes","Marlone",
	"Novaes","Neves","Nunes",
	"Oliveira",
	"Pereira","Pinho","Paiva","Peixoto","Pontes",
	"Queiroz",
	"Ramos","Rocha","Rezende","Rabelo",
	"Santos","Silva","Souza",
	"Tavarez","Tevez","Torres",
	"Vasconcelos","Vascos","Vieira",
	"Xavier"
	).sort();
	
	private static final ListString sobrenomesInvertido;
	
	static {
		sobrenomesInvertido = sobrenomes.copy();
		sobrenomesInvertido.inverteOrdem();
	}
	
	public static String aleatorio() {
		return aleatorio(Aleatorio.getBoolean());
	}
	
	public static String aleatorio(boolean masculino) {
		return Aleatorio.get(masculino ? nomesMasculinos : nomesFemininos) + " " + Aleatorio.get(sobrenomes) + " " + Aleatorio.get(sobrenomes);
	}
	
	private static String mock(ListString itens, int index) {
		while (index >= itens.size()) {
			index -= itens.size();
		}
		return itens.get(index);
	}
	
	public static String mock(int i) {
		return mock(nomes, i) + " " + mock(sobrenomes, i) + " " + sobrenomes.get(sobrenomes.size()-i-1);
	}
	
	public static void main(String[] args) {
		ListString list = sobrenomes.sort();
		String letra = list.get(0).toLowerCase().substring(0, 1);
		while (!list.isEmpty()) {
			String r = list.remove(0);
			String l = r.toLowerCase().substring(0, 1);
			if (!l.equals(letra)) {
				letra = l;
				System.out.println();
			}
			System.out.print("\""+r+"\",");
		}
	}
	
	public static ListString CARACTERES_VALIDOS =
		UConstantes.letrasMinusculas.copy().add(UConstantes.acentuadasMinusculas).add(ListString.array("'", " ", "ç"))
	;

	public static String formatParcial(String s) {

		if (UString.isEmpty(s)) {
			return "";
		}

		s = UString.replaceWhile(s, "\t", " ");

		boolean espaco = s.endsWith(" ");
		s = UString.trimPlus(s);

		while (!UString.isEmpty(s) && s.startsWith("'")) {
			s = s.substring(1);
			s = UString.trimPlus(s);
		}

		if (UString.isEmpty(s)) {
			return "";
		}

		s = s.toLowerCase();
		s = UString.mantemSomenteOsSeguintesCaracteres(s, CARACTERES_VALIDOS);

		if (UString.isEmpty(s)) {
			return "";
		}

		final StringBox box = new StringBox(s);

		CARACTERES_VALIDOS.forEach(o -> {
			box.set(UString.replaceWhile(box.get(), o+o+o, o+o));
		});

		box.set(UString.replaceWhile(box.get(), "''", "'"));
		box.set(" " + box.get() + " ");

		UConstantes.letrasMinusculas.forEach(o -> {
			box.set(UString.replaceWhile(box.get(), " " + o, " " + o.toUpperCase()));
		});

		s = box.get();

		s = UString.replaceWhile(s, " De ", " de ");
		s = UString.replaceWhile(s, " Do ", " do ");
		s = UString.replaceWhile(s, " Da ", " da ");
		s = UString.replaceWhile(s, " Dos ", " dos ");
		s = UString.replaceWhile(s, " Das ", " das ");

		s = UString.trimPlus(s);

		if (UString.length(s) < 2) {
			return s;
		}

		s = s.substring(0,1).toUpperCase() + s.substring(1);

		if (espaco) {
			s += " ";
		}

		return UString.maxLength(s, 60);

	}

	public static boolean isValido(String s) {
		s = UString.trimPlus(s);
		if (UString.length(s) < 7) {
			return false;
		}
		if (!s.contains(" ")) {
			return false;
		}
		return true;
	}	
	
}