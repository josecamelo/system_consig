package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.CepService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class CepControllerAbstract extends ControllerModelo {

	@Autowired
	protected CepService cepService;

	@Override
	protected CepService getService() {
		return cepService;
	}
}
