package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.ClienteTipoService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ClienteTipoControllerAbstract extends ControllerModelo {

	@Autowired
	protected ClienteTipoService clienteTipoService;

	@Override
	protected ClienteTipoService getService() {
		return clienteTipoService;
	}
}
