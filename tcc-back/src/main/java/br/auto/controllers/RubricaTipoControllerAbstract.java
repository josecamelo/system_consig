package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.RubricaTipoService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class RubricaTipoControllerAbstract extends ControllerModelo {

	@Autowired
	protected RubricaTipoService rubricaTipoService;

	@Override
	protected RubricaTipoService getService() {
		return rubricaTipoService;
	}
}
