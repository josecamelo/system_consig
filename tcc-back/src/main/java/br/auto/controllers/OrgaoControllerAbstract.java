package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.OrgaoService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class OrgaoControllerAbstract extends ControllerModelo {

	@Autowired
	protected OrgaoService orgaoService;

	@Override
	protected OrgaoService getService() {
		return orgaoService;
	}
}
