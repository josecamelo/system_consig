package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.ClienteRubricaService;
import gm.utils.map.MapSO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public abstract class ClienteRubricaControllerAbstract extends ControllerModelo {

	@Autowired
	protected ClienteRubricaService clienteRubricaService;

	@Override
	protected ClienteRubricaService getService() {
		return clienteRubricaService;
	}

	@RequestMapping(value="rubrica-lookups", method=RequestMethod.POST)
	public ResponseEntity<Object> rubricaLookups(@RequestBody MapSO map) {
		return ok(map, "lookups", () -> getService().rubricaLookups(map.id()));
	}
}
