package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.RubricaService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class RubricaControllerAbstract extends ControllerModelo {

	@Autowired
	protected RubricaService rubricaService;

	@Override
	protected RubricaService getService() {
		return rubricaService;
	}
}
