package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.ClienteStatusService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ClienteStatusControllerAbstract extends ControllerModelo {

	@Autowired
	protected ClienteStatusService clienteStatusService;

	@Override
	protected ClienteStatusService getService() {
		return clienteStatusService;
	}
}
