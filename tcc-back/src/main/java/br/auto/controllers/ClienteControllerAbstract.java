package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.ClienteService;
import gm.utils.map.MapSO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public abstract class ClienteControllerAbstract extends ControllerModelo {

	@Autowired
	protected ClienteService clienteService;

	@Override
	protected ClienteService getService() {
		return clienteService;
	}

	@RequestMapping(value="cep-lookups", method=RequestMethod.POST)
	public ResponseEntity<Object> cepLookups(@RequestBody MapSO map) {
		return ok(map, "lookups", () -> getService().cepLookups(map.id()));
	}
}
