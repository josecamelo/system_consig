package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.AtendenteService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AtendenteControllerAbstract extends ControllerModelo {

	@Autowired
	protected AtendenteService atendenteService;

	@Override
	protected AtendenteService getService() {
		return atendenteService;
	}
}
