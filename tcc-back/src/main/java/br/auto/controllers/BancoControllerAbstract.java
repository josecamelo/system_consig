package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.BancoService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BancoControllerAbstract extends ControllerModelo {

	@Autowired
	protected BancoService bancoService;

	@Override
	protected BancoService getService() {
		return bancoService;
	}
}
