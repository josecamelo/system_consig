package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.TelefoneService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class TelefoneControllerAbstract extends ControllerModelo {

	@Autowired
	protected TelefoneService telefoneService;

	@Override
	protected TelefoneService getService() {
		return telefoneService;
	}
}
