package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.IndiceService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class IndiceControllerAbstract extends ControllerModelo {

	@Autowired
	protected IndiceService indiceService;

	@Override
	protected IndiceService getService() {
		return indiceService;
	}
}
