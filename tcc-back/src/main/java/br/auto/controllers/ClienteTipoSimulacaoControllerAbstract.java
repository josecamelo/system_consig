package br.auto.controllers;

import br.controllers.ControllerModelo;
import br.impl.service.ClienteTipoSimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ClienteTipoSimulacaoControllerAbstract extends ControllerModelo {

	@Autowired
	protected ClienteTipoSimulacaoService clienteTipoSimulacaoService;

	@Override
	protected ClienteTipoSimulacaoService getService() {
		return clienteTipoSimulacaoService;
	}
}
