package br.auto.model;

import br.impl.outros.EntityModelo;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Banco extends EntityModelo {

	private final Integer id;

	private final String codigo;

	private final String nome;

	@Override
	public void setId(final Integer id) {
		throw new RuntimeException("???");
	}

	@Override
	public Boolean getExcluido() {
		return false;
	}

	@Override
	public Boolean getRegistroBloqueado() {
		return false;
	}

	@Override
	public void setExcluido(final Boolean value) {}

	@Override
	public void setRegistroBloqueado(final Boolean value) {}

	@Override
	public Banco getOld() {
		return (Banco) super.getOld();
	}

	public Banco(final int id, final String codigo, final String nome) {
		this.id = id;
		this.codigo = codigo;
		this.nome = nome;
	}
}
