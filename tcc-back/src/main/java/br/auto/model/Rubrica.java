package br.auto.model;

import br.impl.outros.EntityModelo;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Rubrica extends EntityModelo {

	private final Integer id;

	private final Integer tipo;

	private final String codigo;

	private final String nome;

	@Override
	public void setId(final Integer id) {
		throw new RuntimeException("???");
	}

	@Override
	public Boolean getExcluido() {
		return false;
	}

	@Override
	public Boolean getRegistroBloqueado() {
		return false;
	}

	@Override
	public void setExcluido(final Boolean value) {}

	@Override
	public void setRegistroBloqueado(final Boolean value) {}

	@Override
	public Rubrica getOld() {
		return (Rubrica) super.getOld();
	}

	public Rubrica(final int id, final String codigo, final String nome, final Integer tipo) {
		this.id = id;
		this.codigo = codigo;
		this.nome = nome;
		this.tipo = tipo;
	}
}
