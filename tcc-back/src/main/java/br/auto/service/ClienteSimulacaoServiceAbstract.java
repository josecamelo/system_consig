package br.auto.service;

import br.auto.model.ClienteSimulacao;
import br.auto.select.ClienteSimulacaoSelect;
import br.impl.outros.ResultadoConsulta;
import br.impl.outros.ServiceModelo;
import br.impl.service.ClienteService;
import gm.utils.comum.Lst;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.number.Numeric5;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import java.math.BigDecimal;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ClienteSimulacaoServiceAbstract extends ServiceModelo<ClienteSimulacao> {

	@Autowired
	protected ClienteService clienteService;

	@Override
	public Class<ClienteSimulacao> getClasse() {
		return ClienteSimulacao.class;
	}
	@Override
	public MapSO toMap(final ClienteSimulacao o, final boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getCliente() != null) {
			map.put("cliente", clienteService.toIdText(o.getCliente()));
		}
		if (o.getContratado() != null) {
			map.put("contratado", o.getContratado());
		}
		if (o.getIndice() != null) {
			map.put("indice", new Numeric5(o.getIndice()).toStringPonto());
		}
		if (o.getParcelas() != null) {
			map.put("parcelas", o.getParcelas());
		}
		if (o.getValor() != null) {
			map.put("valor", UString.toString(o.getValor()));
		}
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		map.put("contratar", podeContratar(o));
		return map;
	}
	protected boolean podeContratar(final ClienteSimulacao o) {
		return true;
	}
	@Override
	protected ClienteSimulacao fromMap(MapSO map) {
		final MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		final ClienteSimulacao o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setCliente(find(clienteService, mp, "cliente"));
		return o;
	}
	@Override
	public ClienteSimulacao newO() {
		ClienteSimulacao o = new ClienteSimulacao();
		o.setContratado(false);
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}
	@Override
	protected final void validar(final ClienteSimulacao o) {
		if (o.getCliente() == null) {
			throw new MessageException("O campo Cliente Simulação > Cliente é obrigatório");
		}
		validar2(o);
		if (o.getContratado() == null) {
			throw new MessageException("O campo Cliente Simulação > Contratado é obrigatório");
		}
		o.setIndice(validaDecimal(o.getIndice(), 1, 5, true));
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueParcelasCliente(o);
		}
		validar3(o);
	}
	public void validarUniqueParcelasCliente(final ClienteSimulacao o) {
		if (o.getParcelas() == null) return;
		ClienteSimulacaoSelect<?> select = select();
		if (o.getId() != null) select.ne(o);
		select.parcelas().eq(o.getParcelas());
		select.cliente().eq(o.getCliente());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("clientesimulacao_parcelas_cliente"));
		}
	}
	@Override
	public int getIdEntidade() {
		return IDS.ClienteSimulacao.idEntidade;
	}
	@Override
	public boolean utilizaObservacoes() {
		return false;
	}
	@Override
	public ResultadoConsulta consulta(final MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}
	@Override
	protected ResultadoConsulta consultaBase(final MapSO params, FTT<MapSO, ClienteSimulacao> func) {
		final ClienteSimulacaoSelect<?> select = select();
		Integer pagina = params.getInt("pagina");
		String busca = params.getString("busca");
		if (!UString.isEmpty(busca)) select.busca().like(UString.toCampoBusca(busca));
		ResultadoConsulta result = new ResultadoConsulta();
		if (pagina == null) {
			result.registros = select.count();
		} else {
			select.page(pagina);
		}
		select.limit(30);
		Lst<ClienteSimulacao> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}
	@Override
	protected ClienteSimulacao buscaUnicoObrig(final MapSO params) {
		final ClienteSimulacaoSelect<?> select = select();
		Integer cliente = getId(params, "cliente");
		if (cliente != null) {
			select.cliente().id().eq(cliente);
		}
		Boolean contratado = params.get("contratado");
		if (contratado != null) select.contratado().eq(contratado);
		BigDecimal indice = params.get("indice");
		if (indice != null) select.indice().eq(indice);
		Integer parcelas = params.get("parcelas");
		if (parcelas != null) select.parcelas().eq(parcelas);
		BigDecimal valor = params.get("valor");
		if (valor != null) select.valor().eq(valor);
		ClienteSimulacao o = select.unique();
		if (o != null) {
			return o;
		}
		String s = "";
		if (cliente != null) {
			s += "&& cliente = '" + cliente + "'";
		}
		if (contratado != null) {
			s += "&& contratado = '" + contratado + "'";
		}
		if (indice != null) {
			s += "&& indice = '" + indice + "'";
		}
		if (parcelas != null) {
			s += "&& parcelas = '" + parcelas + "'";
		}
		if (valor != null) {
			s += "&& valor = '" + valor + "'";
		}
		s = "Não foi encontrado um ClienteSimulacao com os seguintes critérios:" + s.substring(2);
		throw new MessageException(s);
	}
	@Override
	public boolean auditar() {
		return false;
	}
	@Override
	protected ClienteSimulacao setOld(final ClienteSimulacao o) {
		ClienteSimulacao old = newO();
		old.setCliente(o.getCliente());
		old.setContratado(o.getContratado());
		old.setIndice(o.getIndice());
		old.setParcelas(o.getParcelas());
		old.setValor(o.getValor());
		o.setOld(old);
		return o;
	}
	public ClienteSimulacaoSelect<?> select(final Boolean excluido) {
		ClienteSimulacaoSelect<?> o = new ClienteSimulacaoSelect<ClienteSimulacaoSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		o.parcelas().asc();
		return o;
	}
	public ClienteSimulacaoSelect<?> select() {
		return select(false);
	}
	@Override
	protected void setBusca(final ClienteSimulacao o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}
	@Override
	public String getText(final ClienteSimulacao o) {
		if (o == null) return null;
		return clienteService.getText(o.getCliente());
	}
	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("ClienteSimulacao");
		list.add("cliente;contratar");
		return list;
	}
	@Transactional
	public void contratar(final int id) {
		contratarImpl(find(id));
	}
	protected abstract void contratarImpl(final ClienteSimulacao o);

	static {
		CONSTRAINTS_MESSAGES.put("clientesimulacao_parcelas_cliente", "A combinação de campos Parcelas + Cliente não pode se repetir. Já existe um registro com esta combinação.");
	}
}
