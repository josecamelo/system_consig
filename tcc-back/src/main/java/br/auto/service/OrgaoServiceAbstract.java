package br.auto.service;

import br.auto.model.Orgao;
import br.impl.outros.ServiceModelo;
import gm.utils.comum.Lst;
import gm.utils.exception.MessageException;
import gm.utils.map.MapSO;
import gm.utils.string.UString;
import java.util.HashMap;
import java.util.Map;

public abstract class OrgaoServiceAbstract extends ServiceModelo<Orgao> {

	public static final int _26201_COLEGIO_PEDRO_II = 1;

	public static final int _21000_COMANDO_DA_AERONAUTICA = 2;

	public static final int _45205_FUND_INST_BRASIL_GEOG_E_ESTATISTICA = 3;

	public static final int _36205_FUNDACAO_NACIONAL_DE_SAUDE = 4;

	public static final int _26277_FUNDACAO_UNIV_FEDERAL_DE_OURO_PRETO = 5;

	public static final int _26274_FUNDACAO_UNIV_FEDERAL_DE_UBERLANDIA = 6;

	public static final int _40803_GOVERNO_DO_EX_TERRITORIO_DE_RONDONIA = 7;

	public static final int _40804_GOVERNO_DO_EX_TERRITORIO_DE_RORAIMA = 8;

	public static final int _40801_GOVERNO_DO_EX_TERRITORIO_DO_AMAPA = 9;

	public static final int _40701_INST_BR_MEIO_AMB_REC_NAT_RENOVAVEIS = 10;

	public static final int _26403_INSTITUTO_FEDERAL_DO_AMAZONAS = 11;

	public static final int _30204_INSTITUTO_NAC_DA_PROPRIEDADE_INDUSTRIAL = 12;

	public static final int _57202_INSTITUTO_NACIONAL_DE_SEGURO_SOCIAL = 13;

	public static final int _40108_MINISTERIO_DA_CIENCIA_E_TECNOLOGIA = 14;

	public static final int _17000_MINISTERIO_DA_FAZENDA = 15;

	public static final int _25000_MINISTERIO_DA_SAUDE = 16;

	public static final int _49000_MINISTERIO_DOS_TRANSPORTES = 17;

	public static final int _40805_ORGAO_40805 = 18;

	public static final int _26269_UNIVERSIDADE_DO_RIO_DE_JANEIRO = 19;

	public static final int _26239_UNIVERSIDADE_FEDERAL_DO_PARA = 20;

	public static final int _26248_UNIVERSIDADE_FEDERAL_RURAL_DE_PERNAMBUCO = 21;

	public static final int _13000_MINIST_DA_AGRICULTURA_PECUARIA_E_ABAST = 22;

	public static final int _15000_MINISTERIO_DA_EDUCACAO = 23;

	public static final int _16000_COMANDO_DO_EXERCITO = 24;

	public static final Map<Integer, Orgao> map = new HashMap<>();

	@Override
	public Class<Orgao> getClasse() {
		return Orgao.class;
	}
	@Override
	public MapSO toMap(final Orgao o, final boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}
	@Override
	protected Orgao fromMap(MapSO map) {
		final MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		if (id == null || id < 1) {
			return null;
		} else {
			return find(id);
		}
	}
	@Override
	public int getIdEntidade() {
		return IDS.Orgao.idEntidade;
	}
	@Override
	public boolean utilizaObservacoes() {
		return false;
	}
	@Override
	protected Orgao buscaUnicoObrig(final MapSO params) {
		String codigo = params.getString("codigo");
		String nome = params.getString("nome");
		Lst<Orgao> list = new Lst<Orgao>();
		list.addAll(map.values());
		Orgao o = list.unique(item -> {
			if (!UString.isEmpty(codigo) && !UString.equals(item.getCodigo(), codigo)) {
				return false;
			}
			if (!UString.isEmpty(nome) && !UString.equals(item.getNome(), nome)) {
				return false;
			}
			return true;
		});
		if (o != null) {
			return o;
		}
		String s = "";
		if (codigo != null) {
			s += "&& codigo = '" + codigo + "'";
		}
		if (nome != null) {
			s += "&& nome = '" + nome + "'";
		}
		s = "Não foi encontrado um Orgao com os seguintes critérios:" + s.substring(2);
		throw new MessageException(s);
	}
	@Override
	public boolean auditar() {
		return false;
	}
	@Override
	public String getText(final Orgao o) {
		if (o == null) return null;
		return o.getNome();
	}
	@Override
	public Orgao findNotObrig(final int id) {
		return map.get(id);
	}

	static {
		map.put(_26201_COLEGIO_PEDRO_II, new Orgao(_26201_COLEGIO_PEDRO_II, "26201", "26201 - COLEGIO PEDRO II"));
		map.put(_21000_COMANDO_DA_AERONAUTICA, new Orgao(_21000_COMANDO_DA_AERONAUTICA, "21000", "21000 - COMANDO DA AERONAUTICA"));
		map.put(_45205_FUND_INST_BRASIL_GEOG_E_ESTATISTICA, new Orgao(_45205_FUND_INST_BRASIL_GEOG_E_ESTATISTICA, "45205", "45205 - FUND, INST, BRASIL, GEOG, E ESTATISTICA"));
		map.put(_36205_FUNDACAO_NACIONAL_DE_SAUDE, new Orgao(_36205_FUNDACAO_NACIONAL_DE_SAUDE, "36205", "36205 - FUNDACAO NACIONAL DE SAUDE"));
		map.put(_26277_FUNDACAO_UNIV_FEDERAL_DE_OURO_PRETO, new Orgao(_26277_FUNDACAO_UNIV_FEDERAL_DE_OURO_PRETO, "26277", "26277 - FUNDACAO UNIV, FEDERAL DE OURO PRETO"));
		map.put(_26274_FUNDACAO_UNIV_FEDERAL_DE_UBERLANDIA, new Orgao(_26274_FUNDACAO_UNIV_FEDERAL_DE_UBERLANDIA, "26274", "26274 - FUNDACAO UNIV, FEDERAL DE UBERLANDIA"));
		map.put(_40803_GOVERNO_DO_EX_TERRITORIO_DE_RONDONIA, new Orgao(_40803_GOVERNO_DO_EX_TERRITORIO_DE_RONDONIA, "40803", "40803 - GOVERNO DO EX-TERRITORIO DE RONDONIA"));
		map.put(_40804_GOVERNO_DO_EX_TERRITORIO_DE_RORAIMA, new Orgao(_40804_GOVERNO_DO_EX_TERRITORIO_DE_RORAIMA, "40804", "40804 - GOVERNO DO EX-TERRITORIO DE RORAIMA"));
		map.put(_40801_GOVERNO_DO_EX_TERRITORIO_DO_AMAPA, new Orgao(_40801_GOVERNO_DO_EX_TERRITORIO_DO_AMAPA, "40801", "40801 - GOVERNO DO EX-TERRITORIO DO AMAPA"));
		map.put(_40701_INST_BR_MEIO_AMB_REC_NAT_RENOVAVEIS, new Orgao(_40701_INST_BR_MEIO_AMB_REC_NAT_RENOVAVEIS, "40701", "40701 - INST, BR, MEIO AMB, REC, NAT, RENOVAVEIS"));
		map.put(_26403_INSTITUTO_FEDERAL_DO_AMAZONAS, new Orgao(_26403_INSTITUTO_FEDERAL_DO_AMAZONAS, "26403", "26403 - INSTITUTO FEDERAL DO AMAZONAS"));
		map.put(_30204_INSTITUTO_NAC_DA_PROPRIEDADE_INDUSTRIAL, new Orgao(_30204_INSTITUTO_NAC_DA_PROPRIEDADE_INDUSTRIAL, "30204", "30204 - INSTITUTO NAC, DA PROPRIEDADE INDUSTRIAL"));
		map.put(_57202_INSTITUTO_NACIONAL_DE_SEGURO_SOCIAL, new Orgao(_57202_INSTITUTO_NACIONAL_DE_SEGURO_SOCIAL, "57202", "57202 - INSTITUTO NACIONAL DE SEGURO SOCIAL"));
		map.put(_40108_MINISTERIO_DA_CIENCIA_E_TECNOLOGIA, new Orgao(_40108_MINISTERIO_DA_CIENCIA_E_TECNOLOGIA, "40108", "40108 - MINISTERIO DA CIENCIA E TECNOLOGIA"));
		map.put(_17000_MINISTERIO_DA_FAZENDA, new Orgao(_17000_MINISTERIO_DA_FAZENDA, "17000", "17000 - MINISTERIO DA FAZENDA"));
		map.put(_25000_MINISTERIO_DA_SAUDE, new Orgao(_25000_MINISTERIO_DA_SAUDE, "25000", "25000 - MINISTERIO DA SAUDE"));
		map.put(_49000_MINISTERIO_DOS_TRANSPORTES, new Orgao(_49000_MINISTERIO_DOS_TRANSPORTES, "49000", "49000 - MINISTERIO DOS TRANSPORTES"));
		map.put(_40805_ORGAO_40805, new Orgao(_40805_ORGAO_40805, "40805", "40805 - ORGAO 40805"));
		map.put(_26269_UNIVERSIDADE_DO_RIO_DE_JANEIRO, new Orgao(_26269_UNIVERSIDADE_DO_RIO_DE_JANEIRO, "26269", "26269 - UNIVERSIDADE DO RIO DE JANEIRO"));
		map.put(_26239_UNIVERSIDADE_FEDERAL_DO_PARA, new Orgao(_26239_UNIVERSIDADE_FEDERAL_DO_PARA, "26239", "26239 - UNIVERSIDADE FEDERAL DO PARA"));
		map.put(_26248_UNIVERSIDADE_FEDERAL_RURAL_DE_PERNAMBUCO, new Orgao(_26248_UNIVERSIDADE_FEDERAL_RURAL_DE_PERNAMBUCO, "26248", "26248 - UNIVERSIDADE FEDERAL RURAL DE PERNAMBUCO"));
		map.put(_13000_MINIST_DA_AGRICULTURA_PECUARIA_E_ABAST, new Orgao(_13000_MINIST_DA_AGRICULTURA_PECUARIA_E_ABAST, "13000", "13000 - MINIST.DA AGRICULTURA,PECUARIA E ABAST."));
		map.put(_15000_MINISTERIO_DA_EDUCACAO, new Orgao(_15000_MINISTERIO_DA_EDUCACAO, "15000", "15000 - MINISTERIO DA EDUCACAO"));
		map.put(_16000_COMANDO_DO_EXERCITO, new Orgao(_16000_COMANDO_DO_EXERCITO, "16000", "16000 - COMANDO DO EXERCITO"));
	}
}
