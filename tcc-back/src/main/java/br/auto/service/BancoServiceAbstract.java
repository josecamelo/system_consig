package br.auto.service;

import br.auto.model.Banco;
import br.impl.outros.ServiceModelo;
import gm.utils.comum.Lst;
import gm.utils.exception.MessageException;
import gm.utils.map.MapSO;
import gm.utils.string.UString;
import java.util.HashMap;
import java.util.Map;

public abstract class BancoServiceAbstract extends ServiceModelo<Banco> {

	public static final int _001_BANCO_DO_BRASIL_S_A_BB = 1;

	public static final int _341_BANCO_ITAU_S_A_ITAU = 2;

	public static final int _033_BANCO_SANTANDER_BRASIL_S_A_SANTANDER = 3;

	public static final int _356_BANCO_REAL_S_A_ANTIGO_REAL = 4;

	public static final int _652_ITAU_UNIBANCO_HOLDING_S_A_ITAU_UNIBANCO = 5;

	public static final int _237_BANCO_BRADESCO_S_A_BRADESCO = 6;

	public static final int _745_BANCO_CITIBANK_S_A_CITIBANK = 7;

	public static final int _399_HSBC_BANK_BRASIL_S_A_BANCO_MULTIPLO_HSBC = 8;

	public static final int _104_CAIXA_ECONOMICA_FEDERAL_CAIXA = 9;

	public static final int _389_BANCO_MERCANTIL_DO_BRASIL_S_A_MERCANTIL = 10;

	public static final int _453_BANCO_RURAL_S_A_RURAL = 11;

	public static final int _422_BANCO_SAFRA_S_A_SAFRA = 12;

	public static final int _633_BANCO_RENDIMENTO_S_A = 13;

	public static final int _246_BANCO_ABC_BRASIL_S_A = 14;

	public static final int _025_BANCO_ALFA_S_A = 15;

	public static final int _641_BANCO_ALVORADA_S_A = 16;

	public static final int _029_BANCO_BANERJ_S_A = 17;

	public static final int _038_BANCO_BANESTADO_S_A = 18;

	public static final int _0_BANCO_BANKPAR_S_A = 19;

	public static final int _740_BANCO_BARCLAYS_S_A = 20;

	public static final int _107_BANCO_BBM_S_A = 21;

	public static final int _031_BANCO_BEG_S_A = 22;

	public static final int _096_BANCO_BM_F_DE_SERVICOS_DE_LIQUIDACAO_E_CUSTODIA_S_A = 23;

	public static final int _318_BANCO_BMG_S_A = 24;

	public static final int _752_BANCO_BNP_PARIBAS_BRASIL_S_A = 25;

	public static final int _248_BANCO_BOAVISTA_INTERATLANTICO_S_A = 26;

	public static final int _036_BANCO_BRADESCO_BBI_S_A = 27;

	public static final int _204_BANCO_BRADESCO_CARTOES_S_A = 28;

	public static final int _225_BANCO_BRASCAN_S_A = 29;

	public static final int _044_BANCO_BVA_S_A = 30;

	public static final int _263_BANCO_CACIQUE_S_A = 31;

	public static final int _473_BANCO_CAIXA_GERAL_BRASIL_S_A = 32;

	public static final int _222_BANCO_CALYON_BRASIL_S_A = 33;

	public static final int _040_BANCO_CARGILL_S_A = 34;

	public static final int M_08_BANCO_CITICARD_S_A = 35;

	public static final int M_19_BANCO_CNH_CAPITAL_S_A = 36;

	public static final int _215_BANCO_COMERCIAL_E_DE_INVESTIMENTO_SUDAMERIS_S_A = 37;

	public static final int _756_BANCO_COOPERATIVO_DO_BRASIL_S_A_BRASIL_S_A = 38;

	public static final int _748_BANCO_COOPERATIVO_SICREDI_S_A = 39;

	public static final int _505_BANCO_CREDIT_SUISSE_BRASIL_S_A = 40;

	public static final int _229_BANCO_CRUZEIRO_DO_SUL_S_A = 41;

	public static final int _003_BANCO_DA_AMAZONIA_S_A = 42;

	public static final int _0833_BANCO_DA_CHINA_BRASIL_S_A = 43;

	public static final int _707_BANCO_DAYCOVAL_S_A = 44;

	public static final int M_06_BANCO_DE_LAGE_LANDEN_BRASIL_S_A = 45;

	public static final int _024_BANCO_DE_PERNAMBUCO_S_A_BANDEPE = 46;

	public static final int _456_BANCO_DE_TOKYO_MITSUBISHI_UFJ_BRASIL_S_A = 47;

	public static final int _214_BANCO_DIBENS_S_A = 48;

	public static final int _047_BANCO_DO_ESTADO_DE_SERGIPE_S_A = 49;

	public static final int _037_BANCO_DO_ESTADO_DO_PARA_S_A = 50;

	public static final int _041_BANCO_DO_ESTADO_DO_RIO_GRANDE_DO_SUL_S_A = 51;

	public static final int _004_BANCO_DO_NORDESTE_DO_BRASIL_S_A = 52;

	public static final int _265_BANCO_FATOR_S_A = 53;

	public static final int M_03_BANCO_FIAT_S_A = 54;

	public static final int _224_BANCO_FIBRA_S_A = 55;

	public static final int _626_BANCO_FICSA_S_A = 56;

	public static final int _394_BANCO_FINASA_BMC_S_A = 57;

	public static final int M_18_BANCO_FORD_S_A = 58;

	public static final int _233_BANCO_GE_CAPITAL_S_A = 59;

	public static final int _734_BANCO_GERDAU_S_A = 60;

	public static final int M_07_BANCO_GMAC_S_A = 61;

	public static final int _612_BANCO_GUANABARA_S_A = 62;

	public static final int M_22_BANCO_HONDA_S_A = 63;

	public static final int _063_BANCO_IBI_S_A_BANCO_MULTIPLO = 64;

	public static final int M_11_BANCO_IBM_S_A = 65;

	public static final int _604_BANCO_INDUSTRIAL_DO_BRASIL_S_A = 66;

	public static final int _320_BANCO_INDUSTRIAL_E_COMERCIAL_S_A = 67;

	public static final int _653_BANCO_INDUSVAL_S_A = 68;

	public static final int _630_BANCO_INTERCAP_S_A = 69;

	public static final int _249_BANCO_INVESTCRED_UNIBANCO_S_A = 70;

	public static final int _184_BANCO_ITAU_BBA_S_A = 71;

	public static final int _479_BANCO_ITAU_BANK_S_A = 72;

	public static final int M_09_BANCO_ITAUCRED_FINANCIAMENTOS_S_A = 73;

	public static final int _376_BANCO_J_P_MORGAN_S_A = 74;

	public static final int _074_BANCO_J_SAFRA_S_A = 75;

	public static final int _217_BANCO_JOHN_DEERE_S_A = 76;

	public static final int _65_BANCO_LEMON_S_A = 77;

	public static final int _600_BANCO_LUSO_BRASILEIRO_S_A = 78;

	public static final int _755_BANCO_MERRILL_LYNCH_DE_INVESTIMENTOS_S_A = 79;

	public static final int _746_BANCO_MODAL_S_A = 80;

	public static final int _151_BANCO_NOSSA_CAIXA_S_A = 81;

	public static final int _45_BANCO_OPPORTUNITY_S_A = 82;

	public static final int _623_BANCO_PANAMERICANO_S_A = 83;

	public static final int _611_BANCO_PAULISTA_S_A = 84;

	public static final int _643_BANCO_PINE_S_A = 85;

	public static final int _638_BANCO_PROSPER_S_A = 86;

	public static final int _747_BANCO_RABOBANK_INTERNATIONAL_BRASIL_S_A = 87;

	public static final int M_16_BANCO_RODOBENS_S_A = 88;

	public static final int _072_BANCO_RURAL_MAIS_S_A = 89;

	public static final int _250_BANCO_SCHAHIN_S_A = 90;

	public static final int _749_BANCO_SIMPLES_S_A = 91;

	public static final int _366_BANCO_SOCIETE_GENERALE_BRASIL_S_A = 92;

	public static final int _637_BANCO_SOFISA_S_A = 93;

	public static final int _464_BANCO_SUMITOMO_MITSUI_BRASILEIRO_S_A = 94;

	public static final int _0825_BANCO_TOPAZIO_S_A = 95;

	public static final int M_20_BANCO_TOYOTA_DO_BRASIL_S_A = 96;

	public static final int _634_BANCO_TRIANGULO_S_A = 97;

	public static final int _208_BANCO_UBS_PACTUAL_S_A = 98;

	public static final int M_14_BANCO_VOLKSWAGEN_S_A = 99;

	public static final int _655_BANCO_VOTORANTIM_S_A = 100;

	public static final int _610_BANCO_VR_S_A = 101;

	public static final int _370_BANCO_WEST_L_B_DO_BRASIL_S_A = 102;

	public static final int _021_BANESTES_S_A_BANCO_DO_ESTADO_DO_ESPIRITO_SANTO_BANESTES = 103;

	public static final int _719_BANIF_BANCO_INTERNACIONAL_DO_FUNCHAL_BRASIL_S_A = 104;

	public static final int _073_BB_BANCO_POPULAR_DO_BRASIL_S_A = 105;

	public static final int _078_BES_INVESTIMENTO_DO_BRASIL_S_A_BANCO_DE_INVESTIMENTO = 106;

	public static final int _069_BPN_BRASIL_BANCO_MULTIPLO_S_A = 107;

	public static final int _070_BANCO_DE_BRASILIA_S_A_BRB = 108;

	public static final int _477_CITIBANK_N_A = 109;

	public static final int _0817_CONCORDIA_BANCO_S_A = 110;

	public static final int _487_DEUTSCHE_BANK_S_A_BANCO_ALEMAO = 111;

	public static final int _751_DRESDNER_BANK_BRASIL_S_A_BANCO_MULTIPLO = 112;

	public static final int _062_HIPERCARD_BANCO_MULTIPLO_S_A = 113;

	public static final int _492_ING_BANK_N_V = 114;

	public static final int _488_J_P_MORGAN_CHASE_BANK = 115;

	public static final int _409_UNIAO_DE_BANCOS_BRASILEIROS_S_A_UNIBANCO = 116;

	public static final int _230_UNICARD_BANCO_MULTIPLO_S_A = 117;

	public static final Map<Integer, Banco> map = new HashMap<>();

	@Override
	public Class<Banco> getClasse() {
		return Banco.class;
	}
	@Override
	public MapSO toMap(final Banco o, final boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}
	@Override
	protected Banco fromMap(MapSO map) {
		final MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		if (id == null || id < 1) {
			return null;
		} else {
			return find(id);
		}
	}
	@Override
	public int getIdEntidade() {
		return IDS.Banco.idEntidade;
	}
	@Override
	public boolean utilizaObservacoes() {
		return false;
	}
	@Override
	protected Banco buscaUnicoObrig(final MapSO params) {
		String codigo = params.getString("codigo");
		String nome = params.getString("nome");
		Lst<Banco> list = new Lst<Banco>();
		list.addAll(map.values());
		Banco o = list.unique(item -> {
			if (!UString.isEmpty(codigo) && !UString.equals(item.getCodigo(), codigo)) {
				return false;
			}
			if (!UString.isEmpty(nome) && !UString.equals(item.getNome(), nome)) {
				return false;
			}
			return true;
		});
		if (o != null) {
			return o;
		}
		String s = "";
		if (codigo != null) {
			s += "&& codigo = '" + codigo + "'";
		}
		if (nome != null) {
			s += "&& nome = '" + nome + "'";
		}
		s = "Não foi encontrado um Banco com os seguintes critérios:" + s.substring(2);
		throw new MessageException(s);
	}
	@Override
	public boolean auditar() {
		return false;
	}
	@Override
	public String getText(final Banco o) {
		if (o == null) return null;
		return o.getNome();
	}
	@Override
	public Banco findNotObrig(final int id) {
		return map.get(id);
	}

	static {
		map.put(_001_BANCO_DO_BRASIL_S_A_BB, new Banco(_001_BANCO_DO_BRASIL_S_A_BB, "001", "001 - Banco do Brasil S.A. (BB)"));
		map.put(_341_BANCO_ITAU_S_A_ITAU, new Banco(_341_BANCO_ITAU_S_A_ITAU, "341", "341 - Banco Itaú S.A. (Itaú)"));
		map.put(_033_BANCO_SANTANDER_BRASIL_S_A_SANTANDER, new Banco(_033_BANCO_SANTANDER_BRASIL_S_A_SANTANDER, "033", "033 - Banco Santander (Brasil) S.A. (Santander)"));
		map.put(_356_BANCO_REAL_S_A_ANTIGO_REAL, new Banco(_356_BANCO_REAL_S_A_ANTIGO_REAL, "356", "356 - Banco Real S.A. (antigo) (Real)"));
		map.put(_652_ITAU_UNIBANCO_HOLDING_S_A_ITAU_UNIBANCO, new Banco(_652_ITAU_UNIBANCO_HOLDING_S_A_ITAU_UNIBANCO, "652", "652 - Itaú Unibanco Holding S.A. (Itaú Unibanco)"));
		map.put(_237_BANCO_BRADESCO_S_A_BRADESCO, new Banco(_237_BANCO_BRADESCO_S_A_BRADESCO, "237", "237 - Banco Bradesco S.A. (Bradesco)"));
		map.put(_745_BANCO_CITIBANK_S_A_CITIBANK, new Banco(_745_BANCO_CITIBANK_S_A_CITIBANK, "745", "745 - Banco Citibank S.A. (Citibank)"));
		map.put(_399_HSBC_BANK_BRASIL_S_A_BANCO_MULTIPLO_HSBC, new Banco(_399_HSBC_BANK_BRASIL_S_A_BANCO_MULTIPLO_HSBC, "399", "399 - HSBC Bank Brasil S.A. - Banco Múltiplo (HSBC)"));
		map.put(_104_CAIXA_ECONOMICA_FEDERAL_CAIXA, new Banco(_104_CAIXA_ECONOMICA_FEDERAL_CAIXA, "104", "104 - Caixa Econômica Federal (Caixa)"));
		map.put(_389_BANCO_MERCANTIL_DO_BRASIL_S_A_MERCANTIL, new Banco(_389_BANCO_MERCANTIL_DO_BRASIL_S_A_MERCANTIL, "389", "389 - Banco Mercantil do Brasil S.A. (Mercantil)"));
		map.put(_453_BANCO_RURAL_S_A_RURAL, new Banco(_453_BANCO_RURAL_S_A_RURAL, "453", "453 - Banco Rural S.A. (Rural)"));
		map.put(_422_BANCO_SAFRA_S_A_SAFRA, new Banco(_422_BANCO_SAFRA_S_A_SAFRA, "422", "422 - Banco Safra S.A. (Safra)"));
		map.put(_633_BANCO_RENDIMENTO_S_A, new Banco(_633_BANCO_RENDIMENTO_S_A, "633", "633 - Banco Rendimento S.A."));
		map.put(_246_BANCO_ABC_BRASIL_S_A, new Banco(_246_BANCO_ABC_BRASIL_S_A, "246", "246 - Banco ABC Brasil S.A."));
		map.put(_025_BANCO_ALFA_S_A, new Banco(_025_BANCO_ALFA_S_A, "025", "025 - Banco Alfa S.A."));
		map.put(_641_BANCO_ALVORADA_S_A, new Banco(_641_BANCO_ALVORADA_S_A, "641", "641 - Banco Alvorada S.A."));
		map.put(_029_BANCO_BANERJ_S_A, new Banco(_029_BANCO_BANERJ_S_A, "029", "029 - Banco Banerj S.A."));
		map.put(_038_BANCO_BANESTADO_S_A, new Banco(_038_BANCO_BANESTADO_S_A, "038", "038 - Banco Banestado S.A."));
		map.put(_0_BANCO_BANKPAR_S_A, new Banco(_0_BANCO_BANKPAR_S_A, "0", "0 - Banco Bankpar S.A."));
		map.put(_740_BANCO_BARCLAYS_S_A, new Banco(_740_BANCO_BARCLAYS_S_A, "740", "740 - Banco Barclays S.A."));
		map.put(_107_BANCO_BBM_S_A, new Banco(_107_BANCO_BBM_S_A, "107", "107 - Banco BBM S.A."));
		map.put(_031_BANCO_BEG_S_A, new Banco(_031_BANCO_BEG_S_A, "031", "031 - Banco Beg S.A."));
		map.put(_096_BANCO_BM_F_DE_SERVICOS_DE_LIQUIDACAO_E_CUSTODIA_S_A, new Banco(_096_BANCO_BM_F_DE_SERVICOS_DE_LIQUIDACAO_E_CUSTODIA_S_A, "096", "096 - Banco BM&F de Serviços de Liquidação e Custódia S.A"));
		map.put(_318_BANCO_BMG_S_A, new Banco(_318_BANCO_BMG_S_A, "318", "318 - Banco BMG S.A."));
		map.put(_752_BANCO_BNP_PARIBAS_BRASIL_S_A, new Banco(_752_BANCO_BNP_PARIBAS_BRASIL_S_A, "752", "752 - Banco BNP Paribas Brasil S.A."));
		map.put(_248_BANCO_BOAVISTA_INTERATLANTICO_S_A, new Banco(_248_BANCO_BOAVISTA_INTERATLANTICO_S_A, "248", "248 - Banco Boavista Interatlântico S.A."));
		map.put(_036_BANCO_BRADESCO_BBI_S_A, new Banco(_036_BANCO_BRADESCO_BBI_S_A, "036", "036 - Banco Bradesco BBI S.A."));
		map.put(_204_BANCO_BRADESCO_CARTOES_S_A, new Banco(_204_BANCO_BRADESCO_CARTOES_S_A, "204", "204 - Banco Bradesco Cartões S.A."));
		map.put(_225_BANCO_BRASCAN_S_A, new Banco(_225_BANCO_BRASCAN_S_A, "225", "225 - Banco Brascan S.A."));
		map.put(_044_BANCO_BVA_S_A, new Banco(_044_BANCO_BVA_S_A, "044", "044 - Banco BVA S.A."));
		map.put(_263_BANCO_CACIQUE_S_A, new Banco(_263_BANCO_CACIQUE_S_A, "263", "263 - Banco Cacique S.A."));
		map.put(_473_BANCO_CAIXA_GERAL_BRASIL_S_A, new Banco(_473_BANCO_CAIXA_GERAL_BRASIL_S_A, "473", "473 - Banco Caixa Geral - Brasil S.A."));
		map.put(_222_BANCO_CALYON_BRASIL_S_A, new Banco(_222_BANCO_CALYON_BRASIL_S_A, "222", "222 - Banco Calyon Brasil S.A."));
		map.put(_040_BANCO_CARGILL_S_A, new Banco(_040_BANCO_CARGILL_S_A, "040", "040 - Banco Cargill S.A."));
		map.put(M_08_BANCO_CITICARD_S_A, new Banco(M_08_BANCO_CITICARD_S_A, "M08", "M08 - Banco Citicard S.A."));
		map.put(M_19_BANCO_CNH_CAPITAL_S_A, new Banco(M_19_BANCO_CNH_CAPITAL_S_A, "M19", "M19 - Banco CNH Capital S.A."));
		map.put(_215_BANCO_COMERCIAL_E_DE_INVESTIMENTO_SUDAMERIS_S_A, new Banco(_215_BANCO_COMERCIAL_E_DE_INVESTIMENTO_SUDAMERIS_S_A, "215", "215 - Banco Comercial e de Investimento Sudameris S.A."));
		map.put(_756_BANCO_COOPERATIVO_DO_BRASIL_S_A_BRASIL_S_A, new Banco(_756_BANCO_COOPERATIVO_DO_BRASIL_S_A_BRASIL_S_A, "756", "756 - Banco Cooperativo do Brasil S.A. - Brasil S.A."));
		map.put(_748_BANCO_COOPERATIVO_SICREDI_S_A, new Banco(_748_BANCO_COOPERATIVO_SICREDI_S_A, "748", "748 - Banco Cooperativo Sicredi S.A."));
		map.put(_505_BANCO_CREDIT_SUISSE_BRASIL_S_A, new Banco(_505_BANCO_CREDIT_SUISSE_BRASIL_S_A, "505", "505 - Banco Credit Suisse (Brasil) S.A."));
		map.put(_229_BANCO_CRUZEIRO_DO_SUL_S_A, new Banco(_229_BANCO_CRUZEIRO_DO_SUL_S_A, "229", "229 - Banco Cruzeiro do Sul S.A."));
		map.put(_003_BANCO_DA_AMAZONIA_S_A, new Banco(_003_BANCO_DA_AMAZONIA_S_A, "003", "003 - Banco da Amazônia S.A."));
		map.put(_0833_BANCO_DA_CHINA_BRASIL_S_A, new Banco(_0833_BANCO_DA_CHINA_BRASIL_S_A, "083-3", "083-3 - Banco da China Brasil S.A."));
		map.put(_707_BANCO_DAYCOVAL_S_A, new Banco(_707_BANCO_DAYCOVAL_S_A, "707", "707 - Banco Daycoval S.A."));
		map.put(M_06_BANCO_DE_LAGE_LANDEN_BRASIL_S_A, new Banco(M_06_BANCO_DE_LAGE_LANDEN_BRASIL_S_A, "M06", "M06 - Banco de Lage Landen Brasil S.A."));
		map.put(_024_BANCO_DE_PERNAMBUCO_S_A_BANDEPE, new Banco(_024_BANCO_DE_PERNAMBUCO_S_A_BANDEPE, "024", "024 - Banco de Pernambuco S.A. - BANDEPE"));
		map.put(_456_BANCO_DE_TOKYO_MITSUBISHI_UFJ_BRASIL_S_A, new Banco(_456_BANCO_DE_TOKYO_MITSUBISHI_UFJ_BRASIL_S_A, "456", "456 - Banco de Tokyo-Mitsubishi UFJ Brasil S.A."));
		map.put(_214_BANCO_DIBENS_S_A, new Banco(_214_BANCO_DIBENS_S_A, "214", "214 - Banco Dibens S.A."));
		map.put(_047_BANCO_DO_ESTADO_DE_SERGIPE_S_A, new Banco(_047_BANCO_DO_ESTADO_DE_SERGIPE_S_A, "047", "047 - Banco do Estado de Sergipe S.A."));
		map.put(_037_BANCO_DO_ESTADO_DO_PARA_S_A, new Banco(_037_BANCO_DO_ESTADO_DO_PARA_S_A, "037", "037 - Banco do Estado do Pará S.A."));
		map.put(_041_BANCO_DO_ESTADO_DO_RIO_GRANDE_DO_SUL_S_A, new Banco(_041_BANCO_DO_ESTADO_DO_RIO_GRANDE_DO_SUL_S_A, "041", "041 - Banco do Estado do Rio Grande do Sul S.A."));
		map.put(_004_BANCO_DO_NORDESTE_DO_BRASIL_S_A, new Banco(_004_BANCO_DO_NORDESTE_DO_BRASIL_S_A, "004", "004 - Banco do Nordeste do Brasil S.A."));
		map.put(_265_BANCO_FATOR_S_A, new Banco(_265_BANCO_FATOR_S_A, "265", "265 - Banco Fator S.A."));
		map.put(M_03_BANCO_FIAT_S_A, new Banco(M_03_BANCO_FIAT_S_A, "M03", "M03 - Banco Fiat S.A."));
		map.put(_224_BANCO_FIBRA_S_A, new Banco(_224_BANCO_FIBRA_S_A, "224", "224 - Banco Fibra S.A."));
		map.put(_626_BANCO_FICSA_S_A, new Banco(_626_BANCO_FICSA_S_A, "626", "626 - Banco Ficsa S.A."));
		map.put(_394_BANCO_FINASA_BMC_S_A, new Banco(_394_BANCO_FINASA_BMC_S_A, "394", "394 - Banco Finasa BMC S.A."));
		map.put(M_18_BANCO_FORD_S_A, new Banco(M_18_BANCO_FORD_S_A, "M18", "M18 - Banco Ford S.A."));
		map.put(_233_BANCO_GE_CAPITAL_S_A, new Banco(_233_BANCO_GE_CAPITAL_S_A, "233", "233 - Banco GE Capital S.A."));
		map.put(_734_BANCO_GERDAU_S_A, new Banco(_734_BANCO_GERDAU_S_A, "734", "734 - Banco Gerdau S.A."));
		map.put(M_07_BANCO_GMAC_S_A, new Banco(M_07_BANCO_GMAC_S_A, "M07", "M07 - Banco GMAC S.A."));
		map.put(_612_BANCO_GUANABARA_S_A, new Banco(_612_BANCO_GUANABARA_S_A, "612", "612 - Banco Guanabara S.A."));
		map.put(M_22_BANCO_HONDA_S_A, new Banco(M_22_BANCO_HONDA_S_A, "M22", "M22 - Banco Honda S.A."));
		map.put(_063_BANCO_IBI_S_A_BANCO_MULTIPLO, new Banco(_063_BANCO_IBI_S_A_BANCO_MULTIPLO, "063", "063 - Banco Ibi S.A. Banco Múltiplo"));
		map.put(M_11_BANCO_IBM_S_A, new Banco(M_11_BANCO_IBM_S_A, "M11", "M11 - Banco IBM S.A."));
		map.put(_604_BANCO_INDUSTRIAL_DO_BRASIL_S_A, new Banco(_604_BANCO_INDUSTRIAL_DO_BRASIL_S_A, "604", "604 - Banco Industrial do Brasil S.A."));
		map.put(_320_BANCO_INDUSTRIAL_E_COMERCIAL_S_A, new Banco(_320_BANCO_INDUSTRIAL_E_COMERCIAL_S_A, "320", "320 - Banco Industrial e Comercial S.A."));
		map.put(_653_BANCO_INDUSVAL_S_A, new Banco(_653_BANCO_INDUSVAL_S_A, "653", "653 - Banco Indusval S.A."));
		map.put(_630_BANCO_INTERCAP_S_A, new Banco(_630_BANCO_INTERCAP_S_A, "630", "630 - Banco Intercap S.A."));
		map.put(_249_BANCO_INVESTCRED_UNIBANCO_S_A, new Banco(_249_BANCO_INVESTCRED_UNIBANCO_S_A, "249", "249 - Banco Investcred Unibanco S.A."));
		map.put(_184_BANCO_ITAU_BBA_S_A, new Banco(_184_BANCO_ITAU_BBA_S_A, "184", "184 - Banco Itaú BBA S.A."));
		map.put(_479_BANCO_ITAU_BANK_S_A, new Banco(_479_BANCO_ITAU_BANK_S_A, "479", "479 - Banco ItaúBank S.A"));
		map.put(M_09_BANCO_ITAUCRED_FINANCIAMENTOS_S_A, new Banco(M_09_BANCO_ITAUCRED_FINANCIAMENTOS_S_A, "M09", "M09 - Banco Itaucred Financiamentos S.A."));
		map.put(_376_BANCO_J_P_MORGAN_S_A, new Banco(_376_BANCO_J_P_MORGAN_S_A, "376", "376 - Banco J. P. Morgan S.A."));
		map.put(_074_BANCO_J_SAFRA_S_A, new Banco(_074_BANCO_J_SAFRA_S_A, "074", "074 - Banco J. Safra S.A."));
		map.put(_217_BANCO_JOHN_DEERE_S_A, new Banco(_217_BANCO_JOHN_DEERE_S_A, "217", "217 - Banco John Deere S.A."));
		map.put(_65_BANCO_LEMON_S_A, new Banco(_65_BANCO_LEMON_S_A, "65", "65 - Banco Lemon S.A."));
		map.put(_600_BANCO_LUSO_BRASILEIRO_S_A, new Banco(_600_BANCO_LUSO_BRASILEIRO_S_A, "600", "600 - Banco Luso Brasileiro S.A."));
		map.put(_755_BANCO_MERRILL_LYNCH_DE_INVESTIMENTOS_S_A, new Banco(_755_BANCO_MERRILL_LYNCH_DE_INVESTIMENTOS_S_A, "755", "755 - Banco Merrill Lynch de Investimentos S.A."));
		map.put(_746_BANCO_MODAL_S_A, new Banco(_746_BANCO_MODAL_S_A, "746", "746 - Banco Modal S.A."));
		map.put(_151_BANCO_NOSSA_CAIXA_S_A, new Banco(_151_BANCO_NOSSA_CAIXA_S_A, "151", "151 - Banco Nossa Caixa S.A."));
		map.put(_45_BANCO_OPPORTUNITY_S_A, new Banco(_45_BANCO_OPPORTUNITY_S_A, "45", "45 - Banco Opportunity S.A."));
		map.put(_623_BANCO_PANAMERICANO_S_A, new Banco(_623_BANCO_PANAMERICANO_S_A, "623", "623 - Banco Panamericano S.A."));
		map.put(_611_BANCO_PAULISTA_S_A, new Banco(_611_BANCO_PAULISTA_S_A, "611", "611 - Banco Paulista S.A."));
		map.put(_643_BANCO_PINE_S_A, new Banco(_643_BANCO_PINE_S_A, "643", "643 - Banco Pine S.A."));
		map.put(_638_BANCO_PROSPER_S_A, new Banco(_638_BANCO_PROSPER_S_A, "638", "638 - Banco Prosper S.A."));
		map.put(_747_BANCO_RABOBANK_INTERNATIONAL_BRASIL_S_A, new Banco(_747_BANCO_RABOBANK_INTERNATIONAL_BRASIL_S_A, "747", "747 - Banco Rabobank International Brasil S.A."));
		map.put(M_16_BANCO_RODOBENS_S_A, new Banco(M_16_BANCO_RODOBENS_S_A, "M16", "M16 - Banco Rodobens S.A."));
		map.put(_072_BANCO_RURAL_MAIS_S_A, new Banco(_072_BANCO_RURAL_MAIS_S_A, "072", "072 - Banco Rural Mais S.A."));
		map.put(_250_BANCO_SCHAHIN_S_A, new Banco(_250_BANCO_SCHAHIN_S_A, "250", "250 - Banco Schahin S.A."));
		map.put(_749_BANCO_SIMPLES_S_A, new Banco(_749_BANCO_SIMPLES_S_A, "749", "749 - Banco Simples S.A."));
		map.put(_366_BANCO_SOCIETE_GENERALE_BRASIL_S_A, new Banco(_366_BANCO_SOCIETE_GENERALE_BRASIL_S_A, "366", "366 - Banco Société Générale Brasil S.A."));
		map.put(_637_BANCO_SOFISA_S_A, new Banco(_637_BANCO_SOFISA_S_A, "637", "637 - Banco Sofisa S.A."));
		map.put(_464_BANCO_SUMITOMO_MITSUI_BRASILEIRO_S_A, new Banco(_464_BANCO_SUMITOMO_MITSUI_BRASILEIRO_S_A, "464", "464 - Banco Sumitomo Mitsui Brasileiro S.A."));
		map.put(_0825_BANCO_TOPAZIO_S_A, new Banco(_0825_BANCO_TOPAZIO_S_A, "082-5", "082-5 - Banco Topázio S.A."));
		map.put(M_20_BANCO_TOYOTA_DO_BRASIL_S_A, new Banco(M_20_BANCO_TOYOTA_DO_BRASIL_S_A, "M20", "M20 - Banco Toyota do Brasil S.A."));
		map.put(_634_BANCO_TRIANGULO_S_A, new Banco(_634_BANCO_TRIANGULO_S_A, "634", "634 - Banco Triângulo S.A."));
		map.put(_208_BANCO_UBS_PACTUAL_S_A, new Banco(_208_BANCO_UBS_PACTUAL_S_A, "208", "208 - Banco UBS Pactual S.A."));
		map.put(M_14_BANCO_VOLKSWAGEN_S_A, new Banco(M_14_BANCO_VOLKSWAGEN_S_A, "M14", "M14 - Banco Volkswagen S.A."));
		map.put(_655_BANCO_VOTORANTIM_S_A, new Banco(_655_BANCO_VOTORANTIM_S_A, "655", "655 - Banco Votorantim S.A."));
		map.put(_610_BANCO_VR_S_A, new Banco(_610_BANCO_VR_S_A, "610", "610 - Banco VR S.A."));
		map.put(_370_BANCO_WEST_L_B_DO_BRASIL_S_A, new Banco(_370_BANCO_WEST_L_B_DO_BRASIL_S_A, "370", "370 - Banco WestLB do Brasil S.A."));
		map.put(_021_BANESTES_S_A_BANCO_DO_ESTADO_DO_ESPIRITO_SANTO_BANESTES, new Banco(_021_BANESTES_S_A_BANCO_DO_ESTADO_DO_ESPIRITO_SANTO_BANESTES, "021", "021 - BANESTES S.A. Banco do Estado do Espírito Santo (BANESTES)"));
		map.put(_719_BANIF_BANCO_INTERNACIONAL_DO_FUNCHAL_BRASIL_S_A, new Banco(_719_BANIF_BANCO_INTERNACIONAL_DO_FUNCHAL_BRASIL_S_A, "719", "719 - Banif-Banco Internacional do Funchal (Brasil)S.A."));
		map.put(_073_BB_BANCO_POPULAR_DO_BRASIL_S_A, new Banco(_073_BB_BANCO_POPULAR_DO_BRASIL_S_A, "073", "073 - BB Banco Popular do Brasil S.A."));
		map.put(_078_BES_INVESTIMENTO_DO_BRASIL_S_A_BANCO_DE_INVESTIMENTO, new Banco(_078_BES_INVESTIMENTO_DO_BRASIL_S_A_BANCO_DE_INVESTIMENTO, "078", "078 - BES Investimento do Brasil S.A.-Banco de Investimento"));
		map.put(_069_BPN_BRASIL_BANCO_MULTIPLO_S_A, new Banco(_069_BPN_BRASIL_BANCO_MULTIPLO_S_A, "069", "069 - BPN Brasil Banco Múltiplo S.A."));
		map.put(_070_BANCO_DE_BRASILIA_S_A_BRB, new Banco(_070_BANCO_DE_BRASILIA_S_A_BRB, "070", "070 - Banco de Brasília S.A. (BRB)"));
		map.put(_477_CITIBANK_N_A, new Banco(_477_CITIBANK_N_A, "477", "477 - Citibank N.A."));
		map.put(_0817_CONCORDIA_BANCO_S_A, new Banco(_0817_CONCORDIA_BANCO_S_A, "081-7", "081-7 - Concórdia Banco S.A."));
		map.put(_487_DEUTSCHE_BANK_S_A_BANCO_ALEMAO, new Banco(_487_DEUTSCHE_BANK_S_A_BANCO_ALEMAO, "487", "487 - Deutsche Bank S.A. - Banco Alemão"));
		map.put(_751_DRESDNER_BANK_BRASIL_S_A_BANCO_MULTIPLO, new Banco(_751_DRESDNER_BANK_BRASIL_S_A_BANCO_MULTIPLO, "751", "751 - Dresdner Bank Brasil S.A. - Banco Múltiplo"));
		map.put(_062_HIPERCARD_BANCO_MULTIPLO_S_A, new Banco(_062_HIPERCARD_BANCO_MULTIPLO_S_A, "062", "062 - Hipercard Banco Múltiplo S.A."));
		map.put(_492_ING_BANK_N_V, new Banco(_492_ING_BANK_N_V, "492", "492 - ING Bank N.V."));
		map.put(_488_J_P_MORGAN_CHASE_BANK, new Banco(_488_J_P_MORGAN_CHASE_BANK, "488", "488 - JPMorgan Chase Bank"));
		map.put(_409_UNIAO_DE_BANCOS_BRASILEIROS_S_A_UNIBANCO, new Banco(_409_UNIAO_DE_BANCOS_BRASILEIROS_S_A_UNIBANCO, "409", "409 - União de Bancos Brasileiros S.A. (UNIBANCO)"));
		map.put(_230_UNICARD_BANCO_MULTIPLO_S_A, new Banco(_230_UNICARD_BANCO_MULTIPLO_S_A, "230", "230 - Unicard Banco Múltiplo S.A."));
	}
}
