package br.auto.service;

import br.auto.model.Cep;
import br.auto.model.Cliente;
import br.auto.model.ClienteRubrica;
import br.auto.model.ClienteSimulacao;
import br.auto.select.ClienteSelect;
import br.impl.outros.ResultadoConsulta;
import br.impl.outros.ServiceModelo;
import br.impl.service.AtendenteService;
import br.impl.service.BancoService;
import br.impl.service.CepService;
import br.impl.service.ClienteRubricaService;
import br.impl.service.ClienteSimulacaoService;
import br.impl.service.ClienteStatusService;
import br.impl.service.ClienteTipoService;
import br.impl.service.ClienteTipoSimulacaoService;
import br.impl.service.IndiceService;
import br.impl.service.OrgaoService;
import br.impl.service.TelefoneService;
import gm.utils.comum.Lst;
import gm.utils.date.Data;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ClienteServiceAbstract extends ServiceModelo<Cliente> {

	@Autowired
	protected AtendenteService atendenteService;

	@Autowired
	protected BancoService bancoService;

	@Autowired
	protected CepService cepService;

	@Autowired
	protected IndiceService indiceService;

	@Autowired
	protected OrgaoService orgaoService;

	@Autowired
	protected ClienteStatusService clienteStatusService;

	@Autowired
	protected TelefoneService telefoneService;

	@Autowired
	protected ClienteTipoService clienteTipoService;

	@Autowired
	protected ClienteTipoSimulacaoService clienteTipoSimulacaoService;

	@Autowired
	protected ClienteRubricaService clienteRubricaService;

	@Autowired
	protected ClienteSimulacaoService clienteSimulacaoService;

	@Override
	public Class<Cliente> getClasse() {
		return Cliente.class;
	}
	@Override
	public MapSO toMap(final Cliente o, final boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getAgencia() != null) {
			map.put("agencia", o.getAgencia());
		}
		if (o.getAtendenteResponsavel() != null) {
			map.put("atendenteResponsavel", atendenteService.toIdText(o.getAtendenteResponsavel()));
		}
		if (o.getBairro() != null) {
			map.put("bairro", o.getBairro());
		}
		if (o.getBanco() != null) {
			map.put("banco", bancoService.toIdText(o.getBanco()));
		}
		if (o.getCep() != null) {
			map.put("cep", cepService.toIdText(o.getCep()));
		}
		if (o.getCidade() != null) {
			map.put("cidade", o.getCidade());
		}
		if (o.getComplemento() != null) {
			map.put("complemento", o.getComplemento());
		}
		if (o.getCpf() != null) {
			map.put("cpf", UString.toString(o.getCpf()));
		}
		if (o.getDataDeNascimento() != null) {
			map.put("dataDeNascimento", new Data(o.getDataDeNascimento()).format("[dd]/[mm]/[yyyy]"));
		}
		if (o.getDia() != null) {
			map.put("dia", indiceService.toIdText(o.getDia()));
		}
		if (o.getEmail() != null) {
			map.put("email", UString.toString(o.getEmail()));
		}
		if (o.getLogradouro() != null) {
			map.put("logradouro", o.getLogradouro());
		}
		if (o.getMargem() != null) {
			map.put("margem", UString.toString(o.getMargem()));
		}
		if (o.getMatricula() != null) {
			map.put("matricula", o.getMatricula());
		}
		if (o.getNome() != null) {
			map.put("nome", UString.toString(o.getNome()));
		}
		if (o.getNumeroDaConta() != null) {
			map.put("numeroDaConta", o.getNumeroDaConta());
		}
		if (o.getOrgao() != null) {
			map.put("orgao", orgaoService.toIdText(o.getOrgao()));
		}
		if (o.getRendaBruta() != null) {
			map.put("rendaBruta", UString.toString(o.getRendaBruta()));
		}
		if (o.getRendaLiquida() != null) {
			map.put("rendaLiquida", UString.toString(o.getRendaLiquida()));
		}
		if (o.getStatus() != null) {
			map.put("status", clienteStatusService.toIdText(o.getStatus()));
		}
		if (o.getTelefonePrincipal() != null) {
			map.put("telefonePrincipal", telefoneService.toMap(o.getTelefonePrincipal(), listas));
		}
		if (o.getTelefoneSecundario() != null) {
			map.put("telefoneSecundario", telefoneService.toMap(o.getTelefoneSecundario(), listas));
		}
		if (o.getTipo() != null) {
			map.put("tipo", clienteTipoService.toIdText(o.getTipo()));
		}
		if (o.getTipoDeSimulacao() != null) {
			map.put("tipoDeSimulacao", clienteTipoSimulacaoService.toIdText(o.getTipoDeSimulacao()));
		}
		if (o.getUf() != null) {
			map.put("uf", o.getUf());
		}
		if (o.getValorDeSimulacao() != null) {
			map.put("valorDeSimulacao", UString.toString(o.getValorDeSimulacao()));
		}
		if (listas) {
			map.put("rubricas", clienteRubricaService.toMapList(getRubricas(o)));
			map.put("simulacoes", clienteSimulacaoService.toMapList(getSimulacoes(o)));
		}
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}
	private Lst<ClienteRubrica> getRubricas(final Cliente o) {
		return clienteRubricaService.select().cliente().eq(o).list();
	}
	private Lst<ClienteSimulacao> getSimulacoes(final Cliente o) {
		return clienteSimulacaoService.select().cliente().eq(o).list();
	}
	@Override
	protected Cliente fromMap(MapSO map) {
		final MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		final Cliente o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setAgencia(mp.getString("agencia"));
		o.setAtendenteResponsavel(find(atendenteService, mp, "atendenteResponsavel"));
		o.setBanco(findId(bancoService, mp, "banco"));
		o.setCep(find(cepService, mp, "cep"));
		o.setComplemento(mp.getString("complemento"));
		o.setCpf(mp.getString("cpf"));
		o.setDataDeNascimento(Data.getCalendar(Data.unformat("[dd]/[mm]/[yyyy]", mp.get("dataDeNascimento"))));
		o.setDia(findId(indiceService, mp, "dia"));
		o.setEmail(mp.getString("email"));
		o.setMatricula(mp.getString("matricula"));
		o.setNome(mp.getString("nome"));
		o.setNumeroDaConta(mp.getString("numeroDaConta"));
		o.setOrgao(findId(orgaoService, mp, "orgao"));
		MapSO telefonePrincipal = mp.get("telefonePrincipal");
		if (telefonePrincipal != null) {
			o.setTelefonePrincipal(telefoneService.save(telefonePrincipal));
		}
		MapSO telefoneSecundario = mp.get("telefoneSecundario");
		if (telefoneSecundario != null) {
			o.setTelefoneSecundario(telefoneService.save(telefoneSecundario));
		}
		o.setTipo(findId(clienteTipoService, mp, "tipo"));
		o.setTipoDeSimulacao(findId(clienteTipoSimulacaoService, mp, "tipoDeSimulacao"));
		o.setValorDeSimulacao(mp.getBigDecimal("valorDeSimulacao"));
		return o;
	}
	@Override
	public Cliente newO() {
		Cliente o = new Cliente();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}
	@Override
	protected final void validar(final Cliente o) {
		o.setAgencia(tratarString(o.getAgencia()));
		if (UString.length(o.getAgencia()) > 50) {
			throw new MessageException("O campo Cliente > Agência aceita no máximo 50 caracteres");
		}
		o.setComplemento(tratarString(o.getComplemento()));
		if (UString.length(o.getComplemento()) > 50) {
			throw new MessageException("O campo Cliente > Complemento aceita no máximo 50 caracteres");
		}
		o.setCpf(tratarCpf(o.getCpf()));
		if (o.getCpf() == null) {
			throw new MessageException("O campo Cliente > CPF é obrigatório");
		}
		if (UString.length(o.getCpf()) > 14) {
			throw new MessageException("O campo Cliente > CPF aceita no máximo 14 caracteres");
		}
		o.setEmail(tratarEmail(o.getEmail()));
		if (UString.length(o.getEmail()) > 50) {
			throw new MessageException("O campo Cliente > E-mail aceita no máximo 50 caracteres");
		}
		o.setMatricula(tratarString(o.getMatricula()));
		if (UString.length(o.getMatricula()) > 50) {
			throw new MessageException("O campo Cliente > Matrícula aceita no máximo 50 caracteres");
		}
		o.setNome(tratarNomeProprio(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Cliente > Nome é obrigatório");
		}
		if (UString.length(o.getNome()) > 50) {
			throw new MessageException("O campo Cliente > Nome aceita no máximo 50 caracteres");
		}
		o.setNumeroDaConta(tratarString(o.getNumeroDaConta()));
		if (UString.length(o.getNumeroDaConta()) > 50) {
			throw new MessageException("O campo Cliente > Número da Conta aceita no máximo 50 caracteres");
		}
		if (o.getTipo() == null) {
			throw new MessageException("O campo Cliente > Tipo é obrigatório");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueCpf(o);
		}
		validar2(o);
		if (o.getStatus() == null) {
			throw new MessageException("O campo Cliente > Status é obrigatório");
		}
		o.setMargem(getMargem(o));
		validar3(o);
	}
	public void validarUniqueCpf(final Cliente o) {
		ClienteSelect<?> select = select();
		if (o.getId() != null) select.ne(o);
		select.cpf().eq(o.getCpf());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("cliente_cpf"));
		}
	}
	protected abstract BigDecimal getMargem(final Cliente o);
	@Override
	public int getIdEntidade() {
		return IDS.Cliente.idEntidade;
	}
	@Override
	protected void saveListas(final Cliente o, final MapSO map) {
		List<MapSO> list;
		final int id = o.getId();
		list = map.get("rubricas");
		if (list != null) {
			for (final MapSO item : list) {
				item.put("cliente", id);
				if (item.isTrue("excluido")) {
					if (!item.isEmpty("id")) {
						clienteRubricaService.delete(item.id());
					}
				} else {
					clienteRubricaService.save(item);
				}
			}
		}
		list = map.get("simulacoes");
		if (list != null) {
			for (final MapSO item : list) {
				item.put("cliente", id);
				if (item.isTrue("excluido")) {
					if (!item.isEmpty("id")) {
						clienteSimulacaoService.delete(item.id());
					}
				} else {
					clienteSimulacaoService.save(item);
				}
			}
		}
	}
	@Override
	public boolean utilizaObservacoes() {
		return false;
	}
	@Override
	public ResultadoConsulta consulta(final MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}
	@Override
	protected ResultadoConsulta consultaBase(final MapSO params, FTT<MapSO, Cliente> func) {
		final ClienteSelect<?> select = select();
		Integer pagina = params.getInt("pagina");
		String busca = params.getString("busca");
		if (!UString.isEmpty(busca)) select.busca().like(UString.toCampoBusca(busca));
		ResultadoConsulta result = new ResultadoConsulta();
		if (pagina == null) {
			result.registros = select.count();
		} else {
			select.page(pagina);
		}
		select.limit(30);
		Lst<Cliente> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}
	@Override
	protected Cliente buscaUnicoObrig(final MapSO params) {
		final ClienteSelect<?> select = select();
		String agencia = params.getString("agencia");
		if (!UString.isEmpty(agencia)) select.agencia().eq(agencia);
		Integer atendenteResponsavel = getId(params, "atendenteResponsavel");
		if (atendenteResponsavel != null) {
			select.atendenteResponsavel().id().eq(atendenteResponsavel);
		}
		Integer banco = getId(params, "banco");
		if (banco != null) {
			select.banco().eq(banco);
		}
		Integer cep = getId(params, "cep");
		if (cep != null) {
			select.cep().id().eq(cep);
		}
		String complemento = params.getString("complemento");
		if (!UString.isEmpty(complemento)) select.complemento().eq(complemento);
		String cpf = params.getString("cpf");
		if (!UString.isEmpty(cpf)) select.cpf().eq(cpf);
		Calendar dataDeNascimento = params.get("dataDeNascimento");
		if (dataDeNascimento != null) select.dataDeNascimento().eq(dataDeNascimento);
		Integer dia = getId(params, "dia");
		if (dia != null) {
			select.dia().eq(dia);
		}
		String email = params.getString("email");
		if (!UString.isEmpty(email)) select.email().eq(email);
		BigDecimal margem = params.get("margem");
		if (margem != null) select.margem().eq(margem);
		String matricula = params.getString("matricula");
		if (!UString.isEmpty(matricula)) select.matricula().eq(matricula);
		String nome = params.getString("nome");
		if (!UString.isEmpty(nome)) select.nome().eq(nome);
		String numeroDaConta = params.getString("numeroDaConta");
		if (!UString.isEmpty(numeroDaConta)) select.numeroDaConta().eq(numeroDaConta);
		Integer orgao = getId(params, "orgao");
		if (orgao != null) {
			select.orgao().eq(orgao);
		}
		BigDecimal rendaBruta = params.get("rendaBruta");
		if (rendaBruta != null) select.rendaBruta().eq(rendaBruta);
		BigDecimal rendaLiquida = params.get("rendaLiquida");
		if (rendaLiquida != null) select.rendaLiquida().eq(rendaLiquida);
		Integer status = getId(params, "status");
		if (status != null) {
			select.status().eq(status);
		}
		Integer telefonePrincipal = getId(params, "telefonePrincipal");
		if (telefonePrincipal != null) {
			select.telefonePrincipal().id().eq(telefonePrincipal);
		}
		Integer telefoneSecundario = getId(params, "telefoneSecundario");
		if (telefoneSecundario != null) {
			select.telefoneSecundario().id().eq(telefoneSecundario);
		}
		Integer tipo = getId(params, "tipo");
		if (tipo != null) {
			select.tipo().eq(tipo);
		}
		Integer tipoDeSimulacao = getId(params, "tipoDeSimulacao");
		if (tipoDeSimulacao != null) {
			select.tipoDeSimulacao().eq(tipoDeSimulacao);
		}
		BigDecimal valorDeSimulacao = params.get("valorDeSimulacao");
		if (valorDeSimulacao != null) select.valorDeSimulacao().eq(valorDeSimulacao);
		Cliente o = select.unique();
		if (o != null) {
			return o;
		}
		String s = "";
		if (agencia != null) {
			s += "&& agencia = '" + agencia + "'";
		}
		if (atendenteResponsavel != null) {
			s += "&& atendenteResponsavel = '" + atendenteResponsavel + "'";
		}
		if (banco != null) {
			s += "&& banco = '" + banco + "'";
		}
		if (cep != null) {
			s += "&& cep = '" + cep + "'";
		}
		if (complemento != null) {
			s += "&& complemento = '" + complemento + "'";
		}
		if (cpf != null) {
			s += "&& cpf = '" + cpf + "'";
		}
		if (dataDeNascimento != null) {
			s += "&& dataDeNascimento = '" + dataDeNascimento + "'";
		}
		if (dia != null) {
			s += "&& dia = '" + dia + "'";
		}
		if (email != null) {
			s += "&& email = '" + email + "'";
		}
		if (margem != null) {
			s += "&& margem = '" + margem + "'";
		}
		if (matricula != null) {
			s += "&& matricula = '" + matricula + "'";
		}
		if (nome != null) {
			s += "&& nome = '" + nome + "'";
		}
		if (numeroDaConta != null) {
			s += "&& numeroDaConta = '" + numeroDaConta + "'";
		}
		if (orgao != null) {
			s += "&& orgao = '" + orgao + "'";
		}
		if (rendaBruta != null) {
			s += "&& rendaBruta = '" + rendaBruta + "'";
		}
		if (rendaLiquida != null) {
			s += "&& rendaLiquida = '" + rendaLiquida + "'";
		}
		if (status != null) {
			s += "&& status = '" + status + "'";
		}
		if (telefonePrincipal != null) {
			s += "&& telefonePrincipal = '" + telefonePrincipal + "'";
		}
		if (telefoneSecundario != null) {
			s += "&& telefoneSecundario = '" + telefoneSecundario + "'";
		}
		if (tipo != null) {
			s += "&& tipo = '" + tipo + "'";
		}
		if (tipoDeSimulacao != null) {
			s += "&& tipoDeSimulacao = '" + tipoDeSimulacao + "'";
		}
		if (valorDeSimulacao != null) {
			s += "&& valorDeSimulacao = '" + valorDeSimulacao + "'";
		}
		s = "Não foi encontrado um Cliente com os seguintes critérios:" + s.substring(2);
		throw new MessageException(s);
	}
	@Override
	public boolean auditar() {
		return false;
	}
	public MapSO cepLookups(final int id) {
		Cep o = cepService.find(id);
		MapSO mp = new MapSO();
		mp.add("bairro", o.getBairro());
		mp.add("cidade", o.getCidade());
		mp.add("logradouro", o.getLogradouro());
		mp.add("uf", o.getUf());
		return mp;
	}
	@Override
	protected Cliente setOld(final Cliente o) {
		Cliente old = newO();
		old.setAgencia(o.getAgencia());
		old.setAtendenteResponsavel(o.getAtendenteResponsavel());
		old.setBanco(o.getBanco());
		old.setCep(o.getCep());
		old.setComplemento(o.getComplemento());
		old.setCpf(o.getCpf());
		old.setDataDeNascimento(o.getDataDeNascimento());
		old.setDia(o.getDia());
		old.setEmail(o.getEmail());
		old.setMargem(o.getMargem());
		old.setMatricula(o.getMatricula());
		old.setNome(o.getNome());
		old.setNumeroDaConta(o.getNumeroDaConta());
		old.setOrgao(o.getOrgao());
		old.setRendaBruta(o.getRendaBruta());
		old.setRendaLiquida(o.getRendaLiquida());
		old.setStatus(o.getStatus());
		old.setTelefonePrincipal(o.getTelefonePrincipal());
		old.setTelefoneSecundario(o.getTelefoneSecundario());
		old.setTipo(o.getTipo());
		old.setTipoDeSimulacao(o.getTipoDeSimulacao());
		old.setValorDeSimulacao(o.getValorDeSimulacao());
		o.setOld(old);
		return o;
	}
	public static boolean statusEmAtendimento(final Cliente o) {
		return o.getStatus() == ClienteStatusServiceAbstract.EM_ATENDIMENTO;
	}
	public boolean statusEmAtendimento(final int id) {
		return statusEmAtendimento(find(id));
	}
	public static boolean statusEmprestimoRealizado(final Cliente o) {
		return o.getStatus() == ClienteStatusServiceAbstract.EMPRESTIMO_REALIZADO;
	}
	public boolean statusEmprestimoRealizado(final int id) {
		return statusEmprestimoRealizado(find(id));
	}
	public static boolean statusNaoTemInteresse(final Cliente o) {
		return o.getStatus() == ClienteStatusServiceAbstract.NAO_TEM_INTERESSE;
	}
	public boolean statusNaoTemInteresse(final int id) {
		return statusNaoTemInteresse(find(id));
	}
	public static boolean tipoServidor(final Cliente o) {
		return o.getTipo() == ClienteTipoServiceAbstract.SERVIDOR;
	}
	public boolean tipoServidor(final int id) {
		return tipoServidor(find(id));
	}
	public static boolean tipoPensionista(final Cliente o) {
		return o.getTipo() == ClienteTipoServiceAbstract.PENSIONISTA;
	}
	public boolean tipoPensionista(final int id) {
		return tipoPensionista(find(id));
	}
	public static boolean tipoDeSimulacaoPeloValorDaParcela(final Cliente o) {
		return o.getTipoDeSimulacao() == ClienteTipoSimulacaoServiceAbstract.PELO_VALOR_DA_PARCELA;
	}
	public boolean tipoDeSimulacaoPeloValorDaParcela(final int id) {
		return tipoDeSimulacaoPeloValorDaParcela(find(id));
	}
	public static boolean tipoDeSimulacaoPeloValorDoEmprestimo(final Cliente o) {
		return o.getTipoDeSimulacao() == ClienteTipoSimulacaoServiceAbstract.PELO_VALOR_DO_EMPRESTIMO;
	}
	public boolean tipoDeSimulacaoPeloValorDoEmprestimo(final int id) {
		return tipoDeSimulacaoPeloValorDoEmprestimo(find(id));
	}
	public ClienteSelect<?> select(final Boolean excluido) {
		ClienteSelect<?> o = new ClienteSelect<ClienteSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		o.nome().asc();
		return o;
	}
	public ClienteSelect<?> select() {
		return select(false);
	}
	@Override
	protected void setBusca(final Cliente o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}
	@Override
	public String getText(final Cliente o) {
		if (o == null) return null;
		String s = "";
		s += " - " + o.getCpf();
		s += " - " + o.getNome();
		return s.substring(3);
	}
	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Cliente");
		list.add("nome;cpf;dataDeNascimento;atendenteResponsavel;tipo;matricula;orgao;banco;agencia;numeroDaConta;telefonePrincipal.ddd;telefonePrincipal.numero;telefonePrincipal.whatsapp;telefonePrincipal.recado;telefoneSecundario.ddd;telefoneSecundario.numero;telefoneSecundario.whatsapp;telefoneSecundario.recado;email;cep;complemento;tipoDeSimulacao;valorDeSimulacao;dia");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("cliente_cpf", "O campo CPF não pode se repetir. Já existe um registro com este valor.");
	}
}
