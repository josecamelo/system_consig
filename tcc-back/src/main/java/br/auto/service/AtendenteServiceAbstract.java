package br.auto.service;

import br.auto.model.Atendente;
import br.auto.select.AtendenteSelect;
import br.impl.outros.ResultadoConsulta;
import br.impl.outros.ServiceModelo;
import gm.utils.comum.Lst;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;

public abstract class AtendenteServiceAbstract extends ServiceModelo<Atendente> {
	@Override
	public Class<Atendente> getClasse() {
		return Atendente.class;
	}
	@Override
	public MapSO toMap(final Atendente o, final boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getEmail() != null) {
			map.put("email", UString.toString(o.getEmail()));
		}
		if (o.getNome() != null) {
			map.put("nome", UString.toString(o.getNome()));
		}
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}
	@Override
	protected Atendente fromMap(MapSO map) {
		final MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		final Atendente o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setEmail(mp.getString("email"));
		o.setNome(mp.getString("nome"));
		return o;
	}
	@Override
	public Atendente newO() {
		Atendente o = new Atendente();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}
	@Override
	protected final void validar(final Atendente o) {
		o.setEmail(tratarEmail(o.getEmail()));
		if (o.getEmail() == null) {
			throw new MessageException("O campo Atendente > E-mail é obrigatório");
		}
		if (UString.length(o.getEmail()) > 50) {
			throw new MessageException("O campo Atendente > E-mail aceita no máximo 50 caracteres");
		}
		o.setNome(tratarNomeProprio(o.getNome()));
		if (o.getNome() == null) {
			throw new MessageException("O campo Atendente > Nome é obrigatório");
		}
		if (UString.length(o.getNome()) > 50) {
			throw new MessageException("O campo Atendente > Nome aceita no máximo 50 caracteres");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueEmail(o);
		}
		validar2(o);
		validar3(o);
	}
	public void validarUniqueEmail(final Atendente o) {
		AtendenteSelect<?> select = select();
		if (o.getId() != null) select.ne(o);
		select.email().eq(o.getEmail());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("atendente_email"));
		}
	}
	@Override
	public int getIdEntidade() {
		return IDS.Atendente.idEntidade;
	}
	@Override
	public boolean utilizaObservacoes() {
		return false;
	}
	@Override
	public ResultadoConsulta consulta(final MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}
	@Override
	protected ResultadoConsulta consultaBase(final MapSO params, FTT<MapSO, Atendente> func) {
		final AtendenteSelect<?> select = select();
		Integer pagina = params.getInt("pagina");
		String busca = params.getString("busca");
		if (!UString.isEmpty(busca)) select.busca().like(UString.toCampoBusca(busca));
		ResultadoConsulta result = new ResultadoConsulta();
		if (pagina == null) {
			result.registros = select.count();
		} else {
			select.page(pagina);
		}
		select.limit(30);
		Lst<Atendente> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}
	@Override
	protected Atendente buscaUnicoObrig(final MapSO params) {
		final AtendenteSelect<?> select = select();
		String email = params.getString("email");
		if (!UString.isEmpty(email)) select.email().eq(email);
		String nome = params.getString("nome");
		if (!UString.isEmpty(nome)) select.nome().eq(nome);
		Integer usuario = getId(params, "usuario");
		if (usuario != null) {
			select.usuario().id().eq(usuario);
		}
		Atendente o = select.unique();
		if (o != null) {
			return o;
		}
		String s = "";
		if (email != null) {
			s += "&& email = '" + email + "'";
		}
		if (nome != null) {
			s += "&& nome = '" + nome + "'";
		}
		if (usuario != null) {
			s += "&& usuario = '" + usuario + "'";
		}
		s = "Não foi encontrado um Atendente com os seguintes critérios:" + s.substring(2);
		throw new MessageException(s);
	}
	@Override
	public boolean auditar() {
		return false;
	}
	@Override
	protected Atendente setOld(final Atendente o) {
		Atendente old = newO();
		old.setEmail(o.getEmail());
		old.setNome(o.getNome());
		old.setUsuario(o.getUsuario());
		o.setOld(old);
		return o;
	}
	public AtendenteSelect<?> select(final Boolean excluido) {
		AtendenteSelect<?> o = new AtendenteSelect<AtendenteSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		o.nome().asc();
		return o;
	}
	public AtendenteSelect<?> select() {
		return select(false);
	}
	@Override
	protected void setBusca(final Atendente o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}
	@Override
	public String getText(final Atendente o) {
		if (o == null) return null;
		return o.getNome();
	}
	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("Atendente");
		list.add("nome;email");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("atendente_email", "O campo E-mail não pode se repetir. Já existe um registro com este valor.");
	}
}
