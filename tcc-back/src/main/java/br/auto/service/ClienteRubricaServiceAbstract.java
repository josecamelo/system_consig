package br.auto.service;

import br.auto.model.ClienteRubrica;
import br.auto.model.Rubrica;
import br.auto.select.ClienteRubricaSelect;
import br.impl.outros.ResultadoConsulta;
import br.impl.outros.ServiceModelo;
import br.impl.service.ClienteService;
import br.impl.service.RubricaService;
import br.impl.service.RubricaTipoService;
import gm.utils.comum.Lst;
import gm.utils.exception.MessageException;
import gm.utils.lambda.FTT;
import gm.utils.map.MapSO;
import gm.utils.string.ListString;
import gm.utils.string.UString;
import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class ClienteRubricaServiceAbstract extends ServiceModelo<ClienteRubrica> {

	@Autowired
	protected ClienteService clienteService;

	@Autowired
	protected RubricaService rubricaService;

	@Autowired
	protected RubricaTipoService rubricaTipoService;

	@Override
	public Class<ClienteRubrica> getClasse() {
		return ClienteRubrica.class;
	}
	@Override
	public MapSO toMap(final ClienteRubrica o, final boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		if (o.getCliente() != null) {
			map.put("cliente", clienteService.toIdText(o.getCliente()));
		}
		if (o.getRubrica() != null) {
			map.put("rubrica", rubricaService.toIdText(o.getRubrica()));
		}
		if (o.getTipo() != null) {
			map.put("tipo", rubricaTipoService.toIdText(o.getTipo()));
		}
		if (o.getValor() != null) {
			map.put("valor", UString.toString(o.getValor()));
		}
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}
	@Override
	protected ClienteRubrica fromMap(MapSO map) {
		final MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		final ClienteRubrica o;
		if (id == null || id < 1) {
			o = newO();
		} else {
			o = find(id);
		}
		o.setCliente(find(clienteService, mp, "cliente"));
		o.setRubrica(findId(rubricaService, mp, "rubrica"));
		o.setValor(mp.getBigDecimal("valor"));
		return o;
	}
	@Override
	public ClienteRubrica newO() {
		ClienteRubrica o = new ClienteRubrica();
		o.setExcluido(false);
		o.setRegistroBloqueado(false);
		return o;
	}
	@Override
	protected final void validar(final ClienteRubrica o) {
		if (o.getCliente() == null) {
			throw new MessageException("O campo Cliente Rubrica > Cliente é obrigatório");
		}
		if (o.getRubrica() == null) {
			throw new MessageException("O campo Cliente Rubrica > Rubrica é obrigatório");
		}
		if (o.getValor() == null) {
			throw new MessageException("O campo Cliente Rubrica > Valor é obrigatório");
		}
		if (!o.isIgnorarUniquesAoPersistir()) {
			validarUniqueRubricaCliente(o);
		}
		validar2(o);
		validar3(o);
	}
	public void validarUniqueRubricaCliente(final ClienteRubrica o) {
		ClienteRubricaSelect<?> select = select();
		if (o.getId() != null) select.ne(o);
		select.rubrica().eq(o.getRubrica());
		select.cliente().eq(o.getCliente());
		if (select.exists()) {
			throw new MessageException(CONSTRAINTS_MESSAGES.get("clienterubrica_rubrica_cliente"));
		}
	}
	@Override
	public int getIdEntidade() {
		return IDS.ClienteRubrica.idEntidade;
	}
	@Override
	public boolean utilizaObservacoes() {
		return false;
	}
	@Override
	public ResultadoConsulta consulta(final MapSO params) {
		return consultaBase(params, o -> toMap(o, false));
	}
	@Override
	protected ResultadoConsulta consultaBase(final MapSO params, FTT<MapSO, ClienteRubrica> func) {
		final ClienteRubricaSelect<?> select = select();
		Integer pagina = params.getInt("pagina");
		String busca = params.getString("busca");
		if (!UString.isEmpty(busca)) select.busca().like(UString.toCampoBusca(busca));
		ResultadoConsulta result = new ResultadoConsulta();
		if (pagina == null) {
			result.registros = select.count();
		} else {
			select.page(pagina);
		}
		select.limit(30);
		Lst<ClienteRubrica> list = select.list();
		result.dados = list.map(o -> func.call(o));
		return result;
	}
	@Override
	protected ClienteRubrica buscaUnicoObrig(final MapSO params) {
		final ClienteRubricaSelect<?> select = select();
		Integer cliente = getId(params, "cliente");
		if (cliente != null) {
			select.cliente().id().eq(cliente);
		}
		Integer rubrica = getId(params, "rubrica");
		if (rubrica != null) {
			select.rubrica().eq(rubrica);
		}
		BigDecimal valor = params.get("valor");
		if (valor != null) select.valor().eq(valor);
		ClienteRubrica o = select.unique();
		if (o != null) {
			return o;
		}
		String s = "";
		if (cliente != null) {
			s += "&& cliente = '" + cliente + "'";
		}
		if (rubrica != null) {
			s += "&& rubrica = '" + rubrica + "'";
		}
		if (valor != null) {
			s += "&& valor = '" + valor + "'";
		}
		s = "Não foi encontrado um ClienteRubrica com os seguintes critérios:" + s.substring(2);
		throw new MessageException(s);
	}
	@Override
	public boolean auditar() {
		return false;
	}
	public MapSO rubricaLookups(final int id) {
		Rubrica o = rubricaService.find(id);
		MapSO mp = new MapSO();
		mp.add("tipo", rubricaTipoService.toIdText(o.getTipo()));
		return mp;
	}
	@Override
	protected ClienteRubrica setOld(final ClienteRubrica o) {
		ClienteRubrica old = newO();
		old.setCliente(o.getCliente());
		old.setRubrica(o.getRubrica());
		old.setValor(o.getValor());
		o.setOld(old);
		return o;
	}
	public static boolean tipoRemuneracao(final ClienteRubrica o) {
		return o.getTipo() == RubricaTipoServiceAbstract.REMUNERACAO;
	}
	public boolean tipoRemuneracao(final int id) {
		return tipoRemuneracao(find(id));
	}
	public static boolean tipoDesconto(final ClienteRubrica o) {
		return o.getTipo() == RubricaTipoServiceAbstract.DESCONTO;
	}
	public boolean tipoDesconto(final int id) {
		return tipoDesconto(find(id));
	}
	public ClienteRubricaSelect<?> select(final Boolean excluido) {
		ClienteRubricaSelect<?> o = new ClienteRubricaSelect<ClienteRubricaSelect<?>>(null, super.criterio(), null);
		if (excluido != null) {
			o.excluido().eq(excluido);
		}
		return o;
	}
	public ClienteRubricaSelect<?> select() {
		return select(false);
	}
	@Override
	protected void setBusca(final ClienteRubrica o) {
		String s = getText(o);
		s = UString.toCampoBusca(s);
		o.setBusca(s);
	}
	@Override
	public String getText(final ClienteRubrica o) {
		if (o == null) return null;
		return clienteService.getText(o.getCliente());
	}
	@Override
	public ListString getTemplateImportacao() {
		ListString list = new ListString();
		list.add("ClienteRubrica");
		list.add("cliente;rubrica;valor");
		return list;
	}

	static {
		CONSTRAINTS_MESSAGES.put("clienterubrica_rubrica_cliente", "A combinação de campos Rubrica + Cliente não pode se repetir. Já existe um registro com esta combinação.");
	}
}
