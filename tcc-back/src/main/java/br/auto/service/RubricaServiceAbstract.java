package br.auto.service;

import br.auto.model.Rubrica;
import br.impl.outros.ServiceModelo;
import gm.utils.comum.Lst;
import gm.utils.exception.MessageException;
import gm.utils.map.MapSO;
import gm.utils.string.UString;
import java.util.HashMap;
import java.util.Map;

public abstract class RubricaServiceAbstract extends ServiceModelo<Rubrica> {

	public static final int _00596_PENSAO_CIVIL = 596;

	public static final int _55986_PENSAO_MILITAR = 55986;

	public static final int _53341_SERVIDOR_CIVIL = 53341;

	public static final int _98020_CONTRIBUICAO_PLANO_SEGURO_SOCIAL_PENSIONISTA = 98020;

	public static final int _99015_IMPOSTO_DE_RENDA_APOSENTADO_PENSIONISTA = 99015;

	public static final int _93389_IRRF_IMPOSTO_DE_RENDA_RETIDO_NA_FONTE = 93389;

	public static final int _95534_PSS_RPGS_PREVIDENCIA_SOCIAL = 95534;

	public static final Map<Integer, Rubrica> map = new HashMap<>();

	@Override
	public Class<Rubrica> getClasse() {
		return Rubrica.class;
	}
	@Override
	public MapSO toMap(final Rubrica o, final boolean listas) {
		MapSO map = new MapSO();
		map.put("id", o.getId());
		map.put("excluido", o.getExcluido());
		map.put("registroBloqueado", o.getRegistroBloqueado());
		return map;
	}
	@Override
	protected Rubrica fromMap(MapSO map) {
		final MapSO mp = new MapSO(map);
		Integer id = mp.getInt("id");
		if (id == null || id < 1) {
			return null;
		} else {
			return find(id);
		}
	}
	@Override
	public int getIdEntidade() {
		return IDS.Rubrica.idEntidade;
	}
	@Override
	public boolean utilizaObservacoes() {
		return false;
	}
	@Override
	protected Rubrica buscaUnicoObrig(final MapSO params) {
		String codigo = params.getString("codigo");
		String nome = params.getString("nome");
		Integer tipo = getId(params, "tipo");
		Lst<Rubrica> list = new Lst<Rubrica>();
		list.addAll(map.values());
		Rubrica o = list.unique(item -> {
			if (!UString.isEmpty(codigo) && !UString.equals(item.getCodigo(), codigo)) {
				return false;
			}
			if (!UString.isEmpty(nome) && !UString.equals(item.getNome(), nome)) {
				return false;
			}
			if (tipo != null && item.getTipo() != tipo) {
				return false;
			}
			return true;
		});
		if (o != null) {
			return o;
		}
		String s = "";
		if (codigo != null) {
			s += "&& codigo = '" + codigo + "'";
		}
		if (nome != null) {
			s += "&& nome = '" + nome + "'";
		}
		if (tipo != null) {
			s += "&& tipo = '" + tipo + "'";
		}
		s = "Não foi encontrado um Rubrica com os seguintes critérios:" + s.substring(2);
		throw new MessageException(s);
	}
	@Override
	public boolean auditar() {
		return false;
	}
	@Override
	public String getText(final Rubrica o) {
		if (o == null) return null;
		return o.getNome();
	}
	@Override
	public Rubrica findNotObrig(final int id) {
		return map.get(id);
	}

	static {
		map.put(_00596_PENSAO_CIVIL, new Rubrica(_00596_PENSAO_CIVIL, "00596", "00596 - Pensão Civil", 1));
		map.put(_55986_PENSAO_MILITAR, new Rubrica(_55986_PENSAO_MILITAR, "55986", "55986 - Pensão Militar", 1));
		map.put(_53341_SERVIDOR_CIVIL, new Rubrica(_53341_SERVIDOR_CIVIL, "53341", "53341 - Servidor Civil", 1));
		map.put(_98020_CONTRIBUICAO_PLANO_SEGURO_SOCIAL_PENSIONISTA, new Rubrica(_98020_CONTRIBUICAO_PLANO_SEGURO_SOCIAL_PENSIONISTA, "98020", "98020 - Contribuição Plano Seguro Social - Pensionista", 2));
		map.put(_99015_IMPOSTO_DE_RENDA_APOSENTADO_PENSIONISTA, new Rubrica(_99015_IMPOSTO_DE_RENDA_APOSENTADO_PENSIONISTA, "99015", "99015 - Imposto de Renda Aposentado/Pensionista", 2));
		map.put(_93389_IRRF_IMPOSTO_DE_RENDA_RETIDO_NA_FONTE, new Rubrica(_93389_IRRF_IMPOSTO_DE_RENDA_RETIDO_NA_FONTE, "93389", "93389 - IRRF - Imposto de Renda Retido na Fonte", 2));
		map.put(_95534_PSS_RPGS_PREVIDENCIA_SOCIAL, new Rubrica(_95534_PSS_RPGS_PREVIDENCIA_SOCIAL, "95534", "95534 - PSS/RPGS - Previdência Social", 2));
	}
}
