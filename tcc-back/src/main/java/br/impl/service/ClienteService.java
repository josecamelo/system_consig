package br.impl.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.auto.model.Atendente;
import br.auto.model.Cliente;
import br.auto.select.ClienteSelect;
import br.auto.service.ClienteServiceAbstract;
import gm.utils.exception.MessageException;
import gm.utils.number.Numeric2;

@Component
public class ClienteService extends ClienteServiceAbstract {
	
	@Autowired BancoService bancoService;
	@Autowired AtendenteService atendenteService;
	@Autowired ClienteSimulacaoService clienteSimulacaoService;
	
	@Override
	public ClienteSelect<?> select() {
		ClienteSelect<?> select = super.select();
		Atendente atendente = atendenteService.getAtendente();
		if (atendente != null) {
			select.atendenteResponsavel().eq(atendente);
		}
		return select;
	}
	
	@Override
	protected void beforeInsert(Cliente o) {
		o.setStatus(ClienteStatusService.EM_ATENDIMENTO);
	}
	
	@Override
	protected void afterUpdate(Cliente o) {
		clienteSimulacaoService.calcularParcelas(o);
	}
	
	@Override
	protected BigDecimal getMargem(Cliente o) {
		Numeric2 remuneracao = new Numeric2(o.getRendaBruta());
		if (remuneracao.isZero()) {
			return null;
		} else {
			Numeric2 valor = remuneracao.vezes(30).dividido(100);
			if (valor.menor(0)) {
				return null;
			} else {
				return valor.getValor();
			}
		}
	}
	
	@Override
	protected void validar3(Cliente o) {
		if (o.getTipoDeSimulacao() == null) {
			return;
		}
		if (tipoDeSimulacaoPeloValorDaParcela(o)) {
			Numeric2 margem = new Numeric2(o.getMargem());
			Numeric2 simulacao = new Numeric2(o.getValorDeSimulacao());
			if (simulacao.maior(margem)) {
				throw new MessageException("Valor de Simulação dever ser menor ou igual à Margem");
			}
		}
	}
	
}