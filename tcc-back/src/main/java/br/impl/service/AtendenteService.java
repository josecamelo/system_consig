package br.impl.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.auto.model.Atendente;
import br.auto.model.Usuario;
import br.auto.service.AtendenteServiceAbstract;
import br.impl.outros.ThreadScope;

@Component
public class AtendenteService extends AtendenteServiceAbstract {
	
	@Autowired Perfis perfis;
	@Autowired UsuarioService usuarioService;
	@Autowired LoginService loginService;
	@Autowired UsuarioPerfilService usuarioPerfilService;

	@Override
	protected void beforeInsertAfterValidate(Atendente o) {
		int id = usuarioService.add(o.getNome(), o.getEmail());
		Usuario usuario = new Usuario();
		usuario.setId(id);
		o.setUsuario(usuario);
	}
	
	@Override
	protected void afterUpdate(Atendente o) {
		if (!o.getEmail().contentEquals(o.getOld().getEmail())) {
			Usuario u = usuarioService.find(o.getUsuario().getId());
			u.setLogin(o.getEmail());
			usuarioService.save(u);
		}
	}
	
	@Override
	protected void afterInsert(Atendente o) {
		usuarioPerfilService.add(o.getUsuario(), perfis.atendente());
	}
	
	public boolean isAtendente() {
		return getAtendente() != null;
	}
	public Atendente getAtendente() {
		Usuario usuario = loginService.find(ThreadScope.getLogin()).getUsuario();
		return select().usuario().eq(usuario).unique();
	}
	
	@Override
	protected void afterDelete(Atendente o) {
		usuarioService.delete(o.getUsuario());
	}
}