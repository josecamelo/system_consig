package br.impl.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.auto.model.Cliente;
import br.auto.model.ClienteRubrica;
import br.auto.service.ClienteRubricaServiceAbstract;
import gm.utils.number.Numeric2;

@Component
public class ClienteRubricaService extends ClienteRubricaServiceAbstract {

	@Autowired ClienteService clienteService;
	
	@Override
	protected void afterInsert(ClienteRubrica o) {
		ajustarRemuneracao(o, null, o.getValor());
	}
	
	@Override
	protected void afterUpdate(ClienteRubrica o) {
		ajustarRemuneracao(o, o.getOld().getValor(), o.getValor());
	}
	
	@Override
	protected void afterDelete(ClienteRubrica o) {
		ajustarRemuneracao(o, o.getValor(), null);
	}
	
	private void ajustarRemuneracao(ClienteRubrica o, BigDecimal velho, BigDecimal novo) {
		
		Cliente cliente = clienteService.find(o.getCliente().getId());
		Numeric2 valor = new Numeric2(cliente.getRendaLiquida());
		if (tipoRemuneracao(o)) {
			valor.add(novo);
			valor.menosIgual(velho);
		} else {
			valor.menosIgual(novo);
			valor.add(velho);
		}
		cliente.setRendaLiquida(valor.getValor());
		
		if (tipoRemuneracao(o)) {
			valor = new Numeric2(cliente.getRendaBruta());
			valor.add(novo);
			valor.menosIgual(velho);
			cliente.setRendaBruta(valor.getValor());
		}
		
		clienteService.save(cliente);
	}
	
}
