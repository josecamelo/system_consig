package br.impl.service;

import br.auto.model.Telefone;
import br.auto.service.TelefoneServiceAbstract;
import gm.utils.outros.UTelefone;

import org.springframework.stereotype.Component;

@Component
public class TelefoneService extends TelefoneServiceAbstract {
	
	@Override
	protected void beforeInsert(Telefone o) {
		setNome(o);
	}
	@Override
	protected void beforeUpdate(Telefone o) {
		setNome(o);
	}
	private void setNome(Telefone o) {
		o.setNome(UTelefone.format(o.getDdd(), o.getNumero()));
	}
	
}
