package br.impl.service;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.auto.model.Cliente;
import br.auto.model.ClienteSimulacao;
import br.auto.model.Indice;
import br.auto.service.ClienteSimulacaoServiceAbstract;
import br.auto.service.IndiceServiceAbstract;
import gm.utils.number.Numeric2;

@Component
public class ClienteSimulacaoService extends ClienteSimulacaoServiceAbstract {

	private static final Indice VAZIO = new Indice(0, null, null, null, null, null, null, null, null, null, null, null, null);
	
	@Autowired ClienteService clienteService;

	public void calcularParcelas(Cliente o) {
		Indice indice = o.getDia() == null ? VAZIO : IndiceServiceAbstract.map.get(o.getDia());
		Numeric2 simulacao = new Numeric2(o.getValorDeSimulacao());
		add(o, 12, simulacao, indice.getEm12());
		add(o, 15, simulacao, indice.getEm15());
		add(o, 18, simulacao, indice.getEm18());
		add(o, 24, simulacao, indice.getEm24());
		add(o, 30, simulacao, indice.getEm30());
		add(o, 36, simulacao, indice.getEm36());
		add(o, 48, simulacao, indice.getEm48());
		add(o, 60, simulacao, indice.getEm60());
		add(o, 72, simulacao, indice.getEm72());
		add(o, 84, simulacao, indice.getEm84());
		add(o, 96, simulacao, indice.getEm96());
	}

	private void add(Cliente cliente, int parcelas, Numeric2 simulacao, BigDecimal indice) {
		ClienteSimulacao o = select(null).cliente().eq(cliente).parcelas().eq(parcelas).unique();
		if (o == null) {
			o = newO();
			o.setParcelas(parcelas);
			o.setCliente(cliente);
		} else {
			setOld(o);
		}
		o.setExcluido(false);
		o.setIndice(indice);
		if (indice == null || simulacao.isZero() || cliente.getTipoDeSimulacao() == null) {
			o.setValor(null);
		} else {
			if (ClienteService.tipoDeSimulacaoPeloValorDaParcela(cliente)) {
				o.setValor(simulacao.dividido(indice).getValor());
			} else if (ClienteService.tipoDeSimulacaoPeloValorDoEmprestimo(cliente)) {
				o.setValor(simulacao.vezes(indice).getValor());
			} else {
				throw new RuntimeException("???");
			}
		}
		save(o);
	}
	
	@Override
	protected boolean podeContratar(ClienteSimulacao o) {
		if (!ClienteService.statusEmAtendimento(o.getCliente())) {
			return false;
		}
		if (o.getValor() == null) {
			return false;
		}
		if (ClienteService.tipoDeSimulacaoPeloValorDoEmprestimo(o.getCliente())) {
			Numeric2 valor = new Numeric2(o.getValor());
			Numeric2 margem = new Numeric2(o.getCliente().getMargem());
			if (valor.maior(margem)) {
				return false;
			}
		}
		return true;
	}

	@Override
	protected void contratarImpl(ClienteSimulacao o) {
		o.setContratado(true);
		save(o);
		Cliente cliente = clienteService.find(o.getCliente().getId());
		cliente.setStatus(ClienteStatusService.EMPRESTIMO_REALIZADO);
		clienteService.save(cliente);
		clienteService.bloqueiaRegistro(o.getCliente());
	}
	
}