package br.impl.controllers;

import br.auto.controllers.IndiceControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="indice/")
public class IndiceController extends IndiceControllerAbstract {}
