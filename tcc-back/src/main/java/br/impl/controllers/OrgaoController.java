package br.impl.controllers;

import br.auto.controllers.OrgaoControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="orgao/")
public class OrgaoController extends OrgaoControllerAbstract {}
