package br.impl.controllers;

import br.auto.controllers.RubricaTipoControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="rubrica-tipo/")
public class RubricaTipoController extends RubricaTipoControllerAbstract {}
