package br.impl.controllers;

import br.auto.controllers.ClienteRubricaControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="cliente-rubrica/")
public class ClienteRubricaController extends ClienteRubricaControllerAbstract {}
