package br.impl.controllers;

import br.auto.controllers.ClienteSimulacaoControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="cliente-simulacao/")
public class ClienteSimulacaoController extends ClienteSimulacaoControllerAbstract {}
