package br.impl.controllers;

import javax.transaction.Transactional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.auto.controllers.CepControllerAbstract;
import br.auto.model.Cep;
import br.impl.service.CepService;
import gm.utils.date.Cronometro;
import gm.utils.string.ListString;

@RestController @RequestMapping(value="cep/")
public class CepController extends CepControllerAbstract {
	
	@RequestMapping(value="teste", method=RequestMethod.GET)
	public ResponseEntity<Object> t1() {
		return ok(() -> {
			return teste();
		});
	}

	@Transactional
	@RequestMapping(value="teste2", method=RequestMethod.GET)
	public ResponseEntity<Object> t2() {
		return ok(() -> {
			return teste();
		});
	}

	private int teste() {
		System.out.println("Start >>>>>>>>>>>>>>>>>>>>>>>>>>>");
		CepService service = getService();
		Cronometro cron = new Cronometro();
		ListString list = new ListString();
		list.load("/opt/desen/gm/cs2019/extras/tcc/tcc-back/src/main/resources/cargas/ceps.csv");
		for (String s : list) {
			Cep o = new Cep();
			o.setNumero(s);
			service.validarUniqueNumero(o);
		}
		return cron.segundos();
	}
	
}
