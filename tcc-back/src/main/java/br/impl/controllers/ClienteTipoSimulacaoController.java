package br.impl.controllers;

import br.auto.controllers.ClienteTipoSimulacaoControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="cliente-tipo-simulacao/")
public class ClienteTipoSimulacaoController extends ClienteTipoSimulacaoControllerAbstract {}
