package br.impl.controllers;

import br.auto.controllers.RubricaControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="rubrica/")
public class RubricaController extends RubricaControllerAbstract {}
