package br.impl.controllers;

import br.auto.controllers.ClienteControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="cliente/")
public class ClienteController extends ClienteControllerAbstract {}
