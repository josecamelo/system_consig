package br.impl.controllers;

import br.auto.controllers.ClienteTipoControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="cliente-tipo/")
public class ClienteTipoController extends ClienteTipoControllerAbstract {}
