package br.impl.controllers;

import br.auto.controllers.TelefoneControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="telefone/")
public class TelefoneController extends TelefoneControllerAbstract {}
