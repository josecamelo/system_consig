package br.impl.controllers;

import br.auto.controllers.ClienteStatusControllerAbstract;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping(value="cliente-status/")
public class ClienteStatusController extends ClienteStatusControllerAbstract {}
