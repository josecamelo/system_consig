package br.outros;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.auto.model.Comando;
import br.auto.model.Entidade;
import br.auto.model.Perfil;
import br.auto.model.PerfilComando;
import br.auto.model.Usuario;
import br.auto.model.UsuarioPerfil;
import br.auto.service.IDS;
import br.auto.service.IDSDefault;
import br.impl.outros.IExec;
import br.impl.service.CepService;
import br.impl.service.ComandoService;
import br.impl.service.EntidadeService;
import br.impl.service.ImportacaoArquivoErroMensagemService;
import br.impl.service.PerfilComandoService;
import br.impl.service.PerfilService;
import br.impl.service.Perfis;
import br.impl.service.UsuarioPerfilService;
import br.impl.service.UsuarioService;
import gm.utils.comum.Lst;

@Component
public class CargaInicial implements IExec {

	@Autowired Perfis perfis;
	@Autowired CepService cepService;
	@Autowired PerfilService perfilService;
	@Autowired UsuarioService usuarioService;
	@Autowired ComandoService comandoService;
	@Autowired EntidadeService entidadeService;
	@Autowired PerfilComandoService perfilComandoService;
	@Autowired UsuarioPerfilService usuarioPerfilService;
	@Autowired ImportacaoArquivoErroMensagemService importacaoArquivoErroMensagemService;
	
	@Override
	public void exec() {

		importacaoArquivoErroMensagemService.cargaInicial();
		
		perfis.cargaInicial();
		
		Perfil perfilAdmin = perfis.administrador();
		Usuario usuarioAdmin = getUsuarioAdmin();
		addAdminAdmin(perfilAdmin, usuarioAdmin);
		addComandos(perfilAdmin, entidadeService.find(IDSDefault.ImportacaoArquivo.idEntidade));
		addComandos(perfilAdmin, entidadeService.find(IDS.Cliente.idEntidade));
		addComandos(perfilAdmin, entidadeService.find(IDS.Atendente.idEntidade));
		addComandos(perfilAdmin, entidadeService.find(IDS.ClienteRubrica.idEntidade));
		addComandos(perfilAdmin, entidadeService.find(IDS.ClienteSimulacao.idEntidade));
		
		Perfil perfilAtendnte = perfis.atendente();
		
		addComandos(perfilAtendnte, comandoService.select()
			.entidade().eq(entidadeService.find(IDS.Cliente.idEntidade))
			.nome().in("read","update")
			.list()
		);
		
	}

	private void addComandos(Perfil perfil, Entidade cliente) {
		Lst<Comando> comandos = comandoService.select().entidade().eq(cliente).list();
		addComandos(perfil, comandos);
	}

	private void addComandos(Perfil perfil, Lst<Comando> comandos) {
		for (Comando comando : comandos) {
			addComando(perfil, comando);
		}
	}

	private void addComando(Perfil perfil, Comando comando) {
		if (!perfilComandoService.select().perfil().eq(perfil).comando().eq(comando).exists()) {
			PerfilComando o = new PerfilComando();
			o.setPerfil(perfil);
			o.setComando(comando);
			perfilComandoService.save(o);
		}
	}

	private Usuario getUsuarioAdmin() {
		Usuario o = usuarioService.findNotObrig(2);
		if (o == null) {
			o = new Usuario();
			o.setExcluido(false);
			o.setRegistroBloqueado(false);
			o.setLogin("admin@admin.com");
			o.setSenha("*");
			o.setNome("Sistema");
			o.setBusca("sistema");
			o.setSenha(UsuarioService.criptografarSenha(2, "123456"));
			usuarioService.save(o);
		}
		return o;
	}
	private void addAdminAdmin(Perfil perfil, Usuario usuario) {
		if (!usuarioPerfilService.select().perfil().eq(perfil).usuario().eq(usuario).exists()) {
			UsuarioPerfil o = new UsuarioPerfil();
			o.setUsuario(usuario);
			o.setPerfil(perfil);
			o.setExcluido(false);
			o.setRegistroBloqueado(false);
			o.setBusca("*");
			usuarioPerfilService.save(o);
		}
	}
	
}